//
//  CredentialsTableViewCell.m
//  iWorld
//
//  Created by Paul Joyce on 30/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CredentialsTableViewCell.h"

@implementation CredentialsTableViewCell

@synthesize label, textField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
