//
//  MBTilesLayerForRMAnnotation.m
//  iWorld
//
//  Created by Paul Joyce on 15/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MBTilesLayerForRMAnnotation.h"

@interface MBTilesLayerForRMAnnotation() {
    RMMapView *rmMapView;
}
@end

@implementation MBTilesLayerForRMAnnotation

- (id)initWithView:(RMMapView *)aMapView {
    if (!(self = [super init]))
        return nil;
    
    rmMapView = aMapView;
    
    return self;
}

- (void)drawInContext:(CGContextRef)theContext {
    NSLog(@"drawInContext");
}

@end
