//
//  OverlayLayer.m
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OverlayLayer.h"

@implementation OverlayLayer

@synthesize name, type, subtype, visible, url, path, fmdb, mkOverlay, rmOverlay;

@end
