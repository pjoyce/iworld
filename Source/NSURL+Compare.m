//
//  NSURL+Compare.m
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSURL+Compare.h"

@implementation NSURL (Compare)

- (BOOL) isEqualToURL:(NSURL*)otherURL {
	return ([[self absoluteURL] isEqual:[otherURL absoluteURL]])
        || ([self isFileURL] && [otherURL isFileURL] &&	[[self path] isEqual:[otherURL path]]);
}

@end
