//
//  CerGeometryFactory.m
//  iWorld
//
//  Created by Lion User on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CerGeometryFactory.h"
#import "CerPointGeometry.h"
#import "CerPolylineGeometry.h"
#import "CerPolygonGeometry.h"

@implementation CerGeometryFactory

// singleton pattern
static CerGeometryFactory *sharedInstance = nil;

+ (void)initialize {
    if(!sharedInstance) {
        sharedInstance = [[CerGeometryFactory alloc] init];
    }
}

+ (CerGeometryFactory *)sharedInstance {
    return sharedInstance;
}

- (id<CerGeometry>) createGeometryFromGeoJSON:(NSString*)json {
    
    id<CerGeometry> geometry;
    
    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSDictionary *dict = [NSJSONSerialization  JSONObjectWithData:jsonData options:0 error:&error];
    if (!error) {
        
        NSString *type = [dict valueForKey:@"type"];
        
        if ([[type lowercaseString] isEqualToString:@"point"]) {
            geometry = [[CerPointGeometry alloc] initFromGeoJSON:json];
            
        } else if ([[type lowercaseString] isEqualToString:@"linestring"]) {
            geometry = [[CerPolylineGeometry alloc] initFromGeoJSON:json];
            
        } else if ([[type lowercaseString] isEqualToString:@"polygon"]) {
            geometry = [[CerPolygonGeometry alloc] initFromGeoJSON:json];
        } else {
            NSLog(@"unhandled geometry type");
        }
        
    }
    
    return geometry;
    
}

@end
