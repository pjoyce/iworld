//
//  OverlayLayersViewController.m
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "OverlayLayersViewController.h"
#import "OverlayLayersModel.h"

@implementation OverlayLayersViewController

@synthesize delegate;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Overlay Layers";
    self.contentSizeForViewInPopover = CGSizeMake(300.0, 280.0);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    return overlayLayersModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:indexPath.row];
    
    cell.textLabel.text = overlayLayer.name;
    if (overlayLayer.visible) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // update the state
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:indexPath.row];
    
    if (overlayLayer.visible) {
        // toggle
        overlayLayer.visible = NO;
        
        // tell the main viewcontroller so it can begin changing the map
        [delegate overlayLayersViewController:self didUnselectLayer:overlayLayer.name];
    } else {        
        // toggle
        overlayLayer.visible = NO;
        
        // tell the main viewcontroller so it can begin changing the map
        [delegate overlayLayersViewController:self didSelectLayer:overlayLayer.name];
    }

    // force areload so cell's checkmark gets updated
    [tableView reloadData];

}


@end
