//
//  CerFeatureAnnotation.m
//  iWorld
//
//  Created by Paul Joyce on 12/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CerFeatureAnnotation.h"
#import "CerPointGeometry.h"
#import "CerPolylineGeometry.h"
#import "CerPolygonGeometry.h"

@implementation CerFeatureAnnotation

@synthesize featureid, geometry, attributes;

- (id) initWithCerFeature:(CerFeature*)feature {
    if (self = [super init]) {
        self.featureid  = feature.featureid;
        //self.layer      = feature.layer;
        self.geometry   = feature.geometry;
        self.attributes = feature.attributes;
    }
    return self;
}

#pragma mark - MKAnnotation

- (CLLocationCoordinate2D)coordinate {
    
    CLLocationCoordinate2D location;
    if ([self.geometry.type isEqualToString:@"point"]) {
        // just use the point itself
        CerPointGeometry *pt = (CerPointGeometry*)self.geometry;
        location.latitude = pt.latitude;
        location.longitude = pt.longitude;
        
    } else if ([self.geometry.type isEqualToString:@"polyline"]) {
        // use a point mid span of the polyline roughly middle edge
        CerPolylineGeometry *pl = (CerPolylineGeometry*)self.geometry;
        NSUInteger m = pl.path.count/2;
        CerPointGeometry *ptA = (CerPointGeometry*)[pl.path objectAtIndex:m];
        CerPointGeometry *ptB = (CerPointGeometry*)[pl.path objectAtIndex:m-1];
        location.latitude = (ptA.latitude + ptB.latitude)/2.0;
        location.longitude = (ptA.longitude + ptB.longitude)/2.0;
        
    } else if ([self.geometry.type isEqualToString:@"polygon"]) {
        // place the annotation callout top centre of the extent box of the geometry
        CerPolygonGeometry *pg = (CerPolygonGeometry*)self.geometry;
        location.latitude = pg.extent.northEast.latitude;
        location.longitude = (pg.extent.northEast.longitude + pg.extent.southWest.longitude)/2.0;
    }
    return location; 
}

- (NSString *)title {
    NSString *myw_title = [self.attributes valueForKey:@"myw_title"];
    return myw_title;
}

- (NSString *)subtitle {
    NSString *myw_short_description = [self.attributes valueForKey:@"myw_short_description"];
    return myw_short_description;
}

@end
