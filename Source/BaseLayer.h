//
//  BaseLayer.h
//  iWorld
//
//  Created by Paul Joyce on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseLayer : NSObject

@property (strong) NSString *name;              // name of layer
@property (strong) NSString *type;              // type of layer
@property (strong) NSString *subtype;           // subtype of layer
@property (strong) NSURL *url;                  // http: or https: url to resource
@property (strong) NSString *path;              // file: path to locally downloaded url if offline
@property          BOOL visible;                // currently visible or not

@end
