//
//  SearchLayer.h
//  iWorld
//
//  Created by Paul Joyce on 30/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface SearchLayer : NSObject

@property (strong) NSString *name;              // name of layer
@property (strong) NSString *type;              // type of layer
@property (strong) NSString *subtype;           // subtype of layer
@property (strong) NSURL *url;                  // http: or https: url to resource
@property (strong) NSString *path;              // file: path to locally downloaded url if offline
@property (strong) FMDatabase *fmdb;            // handle to FMDatabase if offline

@end
