//
//  AppConfiguration.h
//  iWorld
//
//  Created by Paul Joyce on 9/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AppConfiguration : NSObject

+ (AppConfiguration *)sharedInstance;

@property (readonly) int version;                                   // version of config
@property (readonly) CLLocationCoordinate2D homeRegionSouthWest;    // initial region lower left
@property (readonly) CLLocationCoordinate2D homeRegionNorthEast;    // initial region upper right
@property (readonly) NSArray *homeLayers;                           // initial layers on; array of NSStrings
@property (readonly) NSArray *baseLayers;                           // all base layers; array of AppConfigurationItems
@property (readonly) NSArray *overlayLayers;                        // all overlay layers; array of AppConfigurationItems
@property (readonly) NSArray *searchLayers;                         // all search layers; array of AppConfigurationItems

@end
