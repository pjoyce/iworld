//
//  CerPointGeometry.h
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 cerce.net. All rights reserved.
//

#import "CerGeometry.h"
#import "CoreLocation/CoreLocation.h"

@interface CerPointGeometry : NSObject <CerGeometry>
@property CLLocationDegrees longitude;
@property CLLocationDegrees latitude;

- (id) initWithLocation:(CLLocationCoordinate2D)location;

@end
