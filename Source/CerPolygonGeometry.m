//
//  CerPolygonGeometry.m
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CerPolygonGeometry.h"
#import "CerPointGeometry.h"
#import "RMMapView.h"
#import "RMShape.h"

@implementation CerPolygonGeometry

@synthesize path;

- (NSString*) type {
    return @"polygon";
}

- (CerExtent) extent {
	CerExtent e;
    e.northEast.latitude = -HUGE_VAL;
    e.northEast.longitude = -HUGE_VAL;
    e.southWest.latitude = HUGE_VAL;
    e.southWest.longitude = HUGE_VAL;
	for (CerPointGeometry *vertex in path) {
		e.northEast.latitude = MAX(e.northEast.latitude, vertex.latitude);
		e.northEast.longitude = MAX(e.northEast.longitude, vertex.longitude);
		e.southWest.latitude = MIN(e.southWest.latitude, vertex.latitude);
		e.southWest.longitude = MIN(e.southWest.longitude, vertex.longitude);
	}
    return e;
}

- (BOOL) circaInteractsWithLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance {
    /*
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
    // needs attribution
    int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
    {
        int i, j, c = 0;
        for (i = 0, j = nvert-1; i < nvert; j = i++) {
            if ( ((verty[i]>testy) != (verty[j]>testy)) &&
                (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
                c = !c;
        }
        return c;
    }
    */
    BOOL c=NO;
    for (NSUInteger i=0, j=path.count-1; i<path.count; j=i++) {
        CerPointGeometry *A = (CerPointGeometry *)[path objectAtIndex:i];
        CerPointGeometry *B = (CerPointGeometry *)[path objectAtIndex:j];
        if ( ((A.latitude>location.latitude) != (B.latitude>location.latitude)) && 
            (location.longitude < (B.longitude-A.longitude) * (location.latitude-A.latitude) / (B.latitude-A.latitude) + A.longitude) ) {
            c = !c;
        }
    }
    return c;
}

- (CLLocationDegrees) distanceFromLocation:(CLLocationCoordinate2D)location {
    BOOL inside=NO;
    for (NSUInteger i=0, j=path.count-1; i<path.count; j=i++) {
        CerPointGeometry *A = (CerPointGeometry *)[path objectAtIndex:i];
        CerPointGeometry *B = (CerPointGeometry *)[path objectAtIndex:j];
        if ( ((A.latitude>location.latitude) != (B.latitude>location.latitude)) && 
            (location.longitude < (B.longitude-A.longitude) * (location.latitude-A.latitude) / (B.latitude-A.latitude) + A.longitude) ) {
            inside = !inside;
        }
    }
    if (inside) {
        return 0.0;
    }
    // outside, so calculate distance to boundary
    double distanceToLocation = HUGE_VAL;
    for (NSUInteger i=0; i<path.count-1; i++) {
        CerPointGeometry *A = (CerPointGeometry *)[path objectAtIndex:i];
        CerPointGeometry *B = (CerPointGeometry *)[path objectAtIndex:i+1];
        
        double normalLength = sqrt((B.longitude - A.longitude) * (B.longitude - A.longitude) + (B.latitude - A.latitude) * (B.latitude - A.latitude));
        double distanceToAB = fabs((location.longitude - A.longitude) * (B.latitude - A.latitude) - (location.latitude - A.latitude) * (B.longitude - A.longitude)) / normalLength;
        distanceToLocation = MIN(distanceToAB, distanceToLocation);
    }
    return distanceToLocation;
}

- (id) initFromGeoJSON:(NSString*)json {
    if (self = [super init]) {
	
		// {"type": "Polygon", "coordinates": [[[-76.199265281875952, 42.583709061748912], [-76.198648857927992, 42.583728409633864], [-76.198623525436972, 42.583172007700803], [-76.199231056063624, 42.583152957375567], [-76.199265281875952, 42.583709061748912]]]}
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        NSDictionary *dict = [NSJSONSerialization  JSONObjectWithData:jsonData options:0 error:&error];
        if (!error) {
            
            NSString *type = [dict valueForKey:@"type"];
            if ([[type lowercaseString] isEqualToString:@"polygon"]) {
                
				NSMutableArray *mutablePath = [NSMutableArray array];
                NSArray *locus = [dict valueForKey:@"coordinates"];
				for (NSArray *rings in locus) {
                    for (NSArray *coord in rings) {
                        if (coord.count == 2) {
                            CLLocationCoordinate2D location;
                            location.longitude = [[coord objectAtIndex:0] doubleValue];
                            location.latitude  = [[coord objectAtIndex:1] doubleValue];
                            CerPointGeometry *point = [[CerPointGeometry alloc] initWithLocation:location];
                            [mutablePath addObject:point];
                        }
					}
				}
				path = mutablePath;
            }
        } else {
            NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        }
	}
    return self;
    
}

- (id<MKOverlay>) asMKOverlay {
    NSMutableData* data = [NSMutableData dataWithLength:sizeof(CLLocationCoordinate2D) * path.count];
    CLLocationCoordinate2D* array = [data mutableBytes];
    
	for (NSUInteger i=0; i<path.count; i++) {
        CerPointGeometry *vertex = (CerPointGeometry *)[path objectAtIndex:i];
        array[i].latitude = vertex.latitude;
        array[i].longitude = vertex.longitude;
    }
    
    MKPolygon *polygon = [MKPolygon polygonWithCoordinates:array count:path.count];
    return polygon;
}

- (RMMapLayer*) asRMMapLayerForMap:(RMMapView*)rmMapView {
    RMShape *rmpath = [[RMShape alloc] initWithView:rmMapView];
    
    CLLocationCoordinate2D coordinate;
    
	for (NSUInteger i=0; i<path.count; i++) {
        CerPointGeometry *vertex = (CerPointGeometry *)[path objectAtIndex:i];
        coordinate.latitude = vertex.latitude;
        coordinate.longitude = vertex.longitude;
        [rmpath addLineToCoordinate:coordinate];
    }
    [rmpath closePath];
    return rmpath;
}

@end
