//
//  CerPolygonGeometry.h
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CerGeometry.h"

@interface CerPolygonGeometry : NSObject <CerGeometry>

@property (strong) NSArray *path; // array of CerPointGeometry

@end
