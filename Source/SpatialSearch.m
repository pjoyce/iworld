//
//  SpatialSearch.m
//  iWorld
//
//  Created by Paul Joyce on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SpatialSearch.h"
#import "OverlayLayer.h"
#import "OverlayLayersModel.h"
#import "CerFeature.h"
#import "CerGeometryFactory.h"

@implementation SpatialSearch

+ (NSArray *) featuresAtLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance {
    
    NSError *error = nil;
    NSMutableArray *array = [NSMutableArray array];
    
    NSNumber* minx = [NSNumber numberWithDouble:location.longitude - distance];
    NSNumber* maxx = [NSNumber numberWithDouble:location.longitude + distance];

    NSNumber* miny = [NSNumber numberWithDouble:location.latitude - distance];
    NSNumber* maxy = [NSNumber numberWithDouble:location.latitude + distance];
    
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:i];
        if (overlayLayer.fmdb) {
            @synchronized (overlayLayer.fmdb) {
                // wrong/counter-intuitive SQL at first glance. But see http://stackoverflow.com/a/306332
                FMResultSet *results = [overlayLayer.fmdb executeQuery:@"select id from feature_rtree where minx<? and maxx>? and miny<? and maxy>?", maxx, minx, maxy, miny];
                
                // loop over candidate features, checking if it really is a hit, or just a bounding box overlap                
                while ([results next]) {
                    
                    // get the feature record
                    int rowid = [results intForColumnIndex:0];
                    NSLog(@"feature rowid:%d",rowid);
                    FMResultSet *featureRS = [overlayLayer.fmdb executeQuery:@"select id,geometry,properties from feature where rowid=?", [NSNumber numberWithInt:rowid]];
                    
                    if ([featureRS next]) {
                        NSString *featureid  = [featureRS stringForColumnIndex:0];
                        NSString *geometry   = [featureRS stringForColumnIndex:1];
                        NSString *properties = [featureRS stringForColumnIndex:2];
                        
                        CerFeature *feature = [[CerFeature alloc] init];
                        
                        feature.featureid  = featureid;
                        feature.layer      = overlayLayer.name;
                        feature.geometry   = [[CerGeometryFactory sharedInstance] createGeometryFromGeoJSON:geometry];
                        feature.attributes = [NSJSONSerialization JSONObjectWithData:[properties dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];

                        // might be within the bounding box but not close enough
                        double distanceToLocation = [feature.geometry distanceFromLocation:location];
                        NSLog(@"feature distance:%f",distanceToLocation);
                        if (distanceToLocation <= distance) {
                            // [{"feature":feature,"type":type,"distance":distance},{"feature":feature,"type":type,"distance":distance},...]
                            NSDictionary *featureAndTypeAndDistance = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:feature,feature.geometry.type,[NSNumber numberWithDouble:distanceToLocation],nil] 
                                                                                                  forKeys:[NSArray arrayWithObjects:@"feature",@"type",@"distance",nil]];
                            NSLog(@"added to array");
                            [array addObject:featureAndTypeAndDistance];
                        }
                    }
                    [featureRS close];
                }
                
                [results close];
            }
        }
    }
    NSLog(@"found %d candidates",array.count);
    
    // prefer points over lines or polygons, so sort on type ("point" < "polygon" < "polyline"), then on distance
    NSSortDescriptor *descriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"type" ascending:YES];
    NSSortDescriptor *descriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    [array sortUsingDescriptors:[NSArray arrayWithObjects:descriptor1,descriptor2,nil]];

    NSLog(@"sorted");
    
    // make an array of the features in sorted order
    NSMutableArray *features = [NSMutableArray array];
    for (NSDictionary *featureAndDistance in array) {
        //NSLog(@"feature: %@ at \t%.8f",[featureAndDistance objectForKey:@"feature"],[[featureAndDistance objectForKey:@"distance"] doubleValue]);
        [features addObject:[featureAndDistance objectForKey:@"feature"]];
    }
    return features;
}

@end
