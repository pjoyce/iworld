//
//  BaseLayerViewController.h
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BaseLayersViewController;

@protocol BaseLayerViewControllerDelegate
- (void)baseLayerViewController:(BaseLayersViewController *)controller didSelectLayer:(NSString*)layerName;
@end

@interface BaseLayersViewController : UITableViewController

@property (weak, nonatomic) IBOutlet id <BaseLayerViewControllerDelegate> delegate;

@end
