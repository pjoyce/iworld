//
//  MBTilesLayerForRMAnnotation.h
//  iWorld
//
//  Created by Paul Joyce on 15/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RMMapView.h"
#import "RMMapLayer.h"

@interface MBTilesLayerForRMAnnotation : RMMapLayer

- (id)initWithView:(RMMapView *)aMapView;

@end
