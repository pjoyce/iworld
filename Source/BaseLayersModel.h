//
//  BaseLayersModel.h
//  iWorld
//
//  Created by Paul Joyce on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseLayer.h"

@interface BaseLayersModel : NSObject

+ (BaseLayersModel *)sharedInstance;

- (void)reloadData;

- (NSUInteger)count;
- (BaseLayer *)layerAtIndex:(NSUInteger)index;

- (BaseLayer *)layerWithName:(NSString *)name;
- (BaseLayer *)layerWithUrl:(NSURL *)url;

@end
