//
//  SearchLayersModel.h
//  iWorld
//
//  Created by Paul Joyce on 30/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchLayer.h"

@interface SearchLayersModel : NSObject

+ (SearchLayersModel *)sharedInstance;

- (void)reloadData;

- (NSUInteger)count;
- (SearchLayer *)layerAtIndex:(NSUInteger)index;

- (SearchLayer *)layerWithName:(NSString *)name;
- (SearchLayer *)layerWithUrl:(NSURL *)url;

@end
