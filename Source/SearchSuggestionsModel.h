//
//  SearchSuggestionsModel.h
//  iWorld
//
//  Created by Paul Joyce on 26/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CerFeature.h"

@interface SearchSuggestionsModel : NSObject

+ (SearchSuggestionsModel *)sharedInstance;

- (void)suggestionsForString:(NSString *)string;
- (CerFeature*) featureWithDescription:(NSString*)description;

- (NSUInteger)count;
- (NSString *)suggestionAtIndex:(NSUInteger)index;

@end
