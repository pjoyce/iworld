//
//  CerFeatureHighlight.h
//  iWorld
//
//  Created by Paul Joyce on 18/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CerFeatureAnnotation.h"
#import "RMAnnotation.h"
#import "RMMapLayer.h"


@interface CerFeatureHighlight : NSObject <UITableViewDataSource>

@property (strong, readonly) CerFeatureAnnotation *annotation;
@property (strong, readonly) id<MKOverlay> overlay;
@property (strong, readonly) MKOverlayView *overlayView;
@property (strong, readonly) RMAnnotation *rmAnnotation;        // used to display callout bubble
@property (strong, readonly) RMAnnotation *rmAnnotation2;       // used to draw circle/polyline/polygon over feature

@property (strong, readonly) RMMapView *rmMapView;
@property (strong, readonly) MKMapView *mkMapView;


- (id) initWithCerFeature:(CerFeature*)feature andMKMapView:(MKMapView*)mkMapView andRMMapView:(RMMapView*)rmMapView;
- (void) show;
- (void) hide;

@end
