//
//  BaseLayersModel.m
//  iWorld
//
//  Created by Paul Joyce on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseLayersModel.h"
#import "NSURL+Compare.h"
#import "AppConfiguration.h"
#import "AppConfigurationItem.h"
#import "URLBuilder.h"

@interface BaseLayersModel() {
@private
    NSMutableArray *array;
}
@end

@implementation BaseLayersModel

// singleton pattern
static BaseLayersModel *sharedInstance = nil;

+ (void)initialize
{
    if(!sharedInstance) {
        sharedInstance = [[BaseLayersModel alloc] init];
        sharedInstance->array = [NSMutableArray array];
    }
}

+ (BaseLayersModel *)sharedInstance
{
    return sharedInstance;
}

// load the model from the app configuration
- (void)reloadData {
    
    [array removeAllObjects];

    // setup the model
    NSArray *layers = [[AppConfiguration sharedInstance] baseLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
     
        BaseLayer *baseLayer = [[BaseLayer alloc] init];
        baseLayer.name    = layer.name;
        baseLayer.type    = layer.type;
        baseLayer.subtype = layer.subtype;
     
        if (layer.url) {
            if (([baseLayer.subtype isEqualToString:@"mbtiles"] || [baseLayer.subtype isEqualToString:@"mbtiles++"])) {
                // the full source url
                baseLayer.url = [URLBuilder serverURLForConfigurationURL:layer.url];
     
                // was downloaded to this local file:// URL in the documents directory
                baseLayer.path = [[URLBuilder localURLForConfigurationURL:layer.url] path];
            }                
        }
     
        [array addObject:baseLayer];
     }
    
}

// nsarray passthroughs
- (NSUInteger)count {
    return array.count;
}

- (BaseLayer *)layerAtIndex:(NSUInteger)index {
    return [array objectAtIndex:index];
}

- (BaseLayer *)layerWithName:(NSString *)name {
    for (NSUInteger i=0; i<array.count; i++) {
        BaseLayer *BaseLayer = [array objectAtIndex:i];
        if ([BaseLayer.name isEqualToString:name]) {
            return BaseLayer;
        }
    }
    return nil;
}

- (BaseLayer *)layerWithUrl:(NSURL *)url {
    for (NSUInteger i=0; i<array.count; i++) {
        BaseLayer *BaseLayer = [array objectAtIndex:i];
        if ([BaseLayer.url isEqualToURL:url]) {
            return BaseLayer;
        }
    }
    return nil;
}

@end
