//
//  OverlayLayersModel.m
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OverlayLayersModel.h"
#import "NSURL+Compare.h"
#import "AppConfiguration.h"
#import "AppConfigurationItem.h"
#import "URLBuilder.h"

@interface OverlayLayersModel() {
@private
    NSMutableArray *array;
}
@end

@implementation OverlayLayersModel

// singleton pattern
static OverlayLayersModel *sharedInstance = nil;

+ (void)initialize
{
    if(!sharedInstance) {
        sharedInstance = [[OverlayLayersModel alloc] init];
        sharedInstance->array = [NSMutableArray array];
    }
}

+ (OverlayLayersModel *)sharedInstance
{
    return sharedInstance;
}

// load the model from the app configuration
- (void)reloadData {

    [array removeAllObjects];
    
    // setup the model
    NSArray *layers = [[AppConfiguration sharedInstance] overlayLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
        
        OverlayLayer *overlayLayer = [[OverlayLayer alloc] init];
        overlayLayer.name    = layer.name;
        overlayLayer.type    = layer.type;
        overlayLayer.subtype = layer.subtype;
        
        if (layer.url) {
            if (([overlayLayer.subtype isEqualToString:@"mbtiles"] || [overlayLayer.subtype isEqualToString:@"mbtiles++"])) {
                // the full source url
                overlayLayer.url = [URLBuilder serverURLForConfigurationURL:layer.url];
                
                // was downloaded to this local file:// URL in the documents directory
                overlayLayer.path = [[URLBuilder localURLForConfigurationURL:layer.url] path];

                // open the database
                overlayLayer.fmdb = [FMDatabase databaseWithPath:overlayLayer.path];
                [overlayLayer.fmdb open];
                NSLog(@"opened database %@ %@",overlayLayer.path, overlayLayer.fmdb.goodConnection?@"YES":@"NO");

            }                
        }
        
        [array addObject:overlayLayer];
    }
    
}

// nsarray passthroughs
- (NSUInteger)count {
    return array.count;
}

- (OverlayLayer *)layerAtIndex:(NSUInteger)index {
    return [array objectAtIndex:index];
}

- (OverlayLayer *)layerWithName:(NSString *)name {
    for (NSUInteger i=0; i<array.count; i++) {
        OverlayLayer *overlayLayer = [array objectAtIndex:i];
        if ([overlayLayer.name isEqualToString:name]) {
            return overlayLayer;
        }
    }
    return nil;
}

- (OverlayLayer *)layerWithUrl:(NSURL *)url {
    for (NSUInteger i=0; i<array.count; i++) {
        OverlayLayer *overlayLayer = [array objectAtIndex:i];
        if ([overlayLayer.url isEqualToURL:url]) {
            return overlayLayer;
        }
    }
    return nil;
}

@end
