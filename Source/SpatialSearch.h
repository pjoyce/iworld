//
//  SpatialSearch.h
//  iWorld
//
//  Created by Paul Joyce on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SpatialSearch : NSObject

+ (NSArray *) featuresAtLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance;

@end
