//
//  BaseLayerViewController.m
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseLayersViewController.h"
#import "BaseLayersModel.h"

@implementation BaseLayersViewController

@synthesize delegate;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Base Layers";
    self.contentSizeForViewInPopover = CGSizeMake(300.0, 280.0);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    return baseLayersModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    BaseLayer *baseLayer = [baseLayersModel layerAtIndex:indexPath.row];
    
    cell.textLabel.text = baseLayer.name;
    if (baseLayer.visible) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // update the model
    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    for (NSUInteger i=0; i<baseLayersModel.count; i++) {
        [[baseLayersModel layerAtIndex:i] setVisible:NO];
    }
    
    BaseLayer *baseLayer = [baseLayersModel layerAtIndex:indexPath.row];
    baseLayer.visible = YES;
    
    // force a reload so other cells unselected
    [tableView reloadData];
    
    // tell the main viewcontroller so it can begin changing the map
    [delegate baseLayerViewController:self didSelectLayer:baseLayer.name];
}

@end
