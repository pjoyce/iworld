//
//  SearchSuggestionsModel.m
//  iWorld
//
//  Created by Paul Joyce on 26/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SearchSuggestionsModel.h"
#import "OverlayLayer.h"
#import "OverlayLayersModel.h"
#import "SearchLayer.h"
#import "SearchLayersModel.h"
#import "CerGeometryFactory.h"

@interface SearchSuggestionsModel() {
@private
    NSMutableArray *array;
}
@end

@implementation SearchSuggestionsModel

// singleton pattern
static SearchSuggestionsModel *sharedInstance = nil;

+ (void)initialize {
    if(!sharedInstance) {
        sharedInstance = [[SearchSuggestionsModel alloc] init];
        sharedInstance->array = [NSMutableArray array];
    }
}

+ (SearchSuggestionsModel *)sharedInstance {
    return sharedInstance;
}


-(void)suggestionsForString:(NSString *)string {
    [array removeAllObjects];
    
    NSString *pattern = [string stringByAppendingString:@"*"];
    
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:i];
        if (overlayLayer.fmdb) {
            @synchronized (overlayLayer.fmdb) {
                FMResultSet *results = [overlayLayer.fmdb executeQuery:@"select desc1,desc2 from feature_fts where feature_fts match ?", pattern];
                NSUInteger count = 0;
                while ([results next] && count++<50) {
                    NSString *desc1 = [results stringForColumnIndex:0];
                    NSString *desc2 = [results stringForColumnIndex:1];
                    
                    NSRange range = [desc1.lowercaseString rangeOfString:string.lowercaseString];
                    if (range.location != NSNotFound) {
                        [array addObject:desc1];
                    } else {
                        [array addObject:desc2];
                    }
                }
                [results close];
            }
        }
    }

    if (array.count == 0) {
        SearchLayersModel *searchLayersModel = [SearchLayersModel sharedInstance];
        for (NSUInteger i=0; i<searchLayersModel.count; i++) {
            SearchLayer *searchLayer = [searchLayersModel layerAtIndex:i];
            if (searchLayer.fmdb) {
                @synchronized (searchLayer.fmdb) {
                    FMResultSet *results = [searchLayer.fmdb executeQuery:@"select desc from search_fts where search_fts match ?", pattern];
                    NSUInteger count = 0;
                    while ([results next] && count++<50) {
                        NSString *desc = [results stringForColumnIndex:0];
                        [array addObject:desc];
                    }
                    [results close];
                }
            } else {
                // do online search if reachable
            }
        }
    }
}

// a string has been selected from the search suggestions list (returned by above method)
// need to find the record it came from
- (CerFeature*) featureWithDescription:(NSString*)description {

    // try the overlay layers first
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:i];
        if (overlayLayer.fmdb) {
            @synchronized (overlayLayer.fmdb) {
                FMResultSet *results = [overlayLayer.fmdb executeQuery:@"select rowid from feature_fts where feature_fts match ?", description];
                if ([results next]) {
                    int rowid = [results intForColumnIndex:0];

                    FMResultSet *featureRS = [overlayLayer.fmdb executeQuery:@"select id,geometry,properties from feature where rowid=?", [NSNumber numberWithInt:rowid]];
                    
                    if ([featureRS next]) {
                        NSString *featureid  = [featureRS stringForColumnIndex:0];
                        NSString *geometry   = [featureRS stringForColumnIndex:1];
                        NSString *properties = [featureRS stringForColumnIndex:2];
                        
                        CerFeature *feature = [[CerFeature alloc] init];
                        
                        feature.featureid  = featureid;
                        feature.layer      = overlayLayer.name;
                        feature.geometry   = [[CerGeometryFactory sharedInstance] createGeometryFromGeoJSON:geometry];
                        feature.attributes = [NSJSONSerialization JSONObjectWithData:[properties dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
                        return feature;
                    }
                }
                [results close];
            }
        }
    }

    // next try the search layers
    SearchLayersModel *searchLayersModel = [SearchLayersModel sharedInstance];
    for (NSUInteger i=0; i<searchLayersModel.count; i++) {
        SearchLayer *searchLayer = [searchLayersModel layerAtIndex:i];
        if (searchLayer.fmdb) {
            @synchronized (searchLayer.fmdb) {
                FMResultSet *results = [searchLayer.fmdb executeQuery:@"select rowid from search_fts where search_fts match ?", description];
                if ([results next]) {
                    int rowid = [results intForColumnIndex:0];
                    
                    FMResultSet *featureRS = [searchLayer.fmdb executeQuery:@"select id,geometry,properties from search where rowid=?", [NSNumber numberWithInt:rowid]];
                    
                    if ([featureRS next]) {
                        NSString *featureid  = [featureRS stringForColumnIndex:0];
                        NSString *geometry   = [featureRS stringForColumnIndex:1];
                        NSString *properties = [featureRS stringForColumnIndex:2];
                        
                        CerFeature *feature = [[CerFeature alloc] init];
                        
                        feature.featureid  = featureid;
                        feature.layer      = searchLayer.name;
                        feature.geometry   = [[CerGeometryFactory sharedInstance] createGeometryFromGeoJSON:geometry];
                        feature.attributes = [NSJSONSerialization JSONObjectWithData:[properties dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
                        return feature;
                    }
                }
                [results close];
            }
        }
    }
    
    
    // should never happen
    return nil;
}

- (NSUInteger)count {
    return array.count;
}

- (NSString *)suggestionAtIndex:(NSUInteger)index {
    return [array objectAtIndex:index];
}

@end
