//
//  MKMapView+FeatureHighlight.h
//  iWorld
//
//  Created by Paul Joyce on 24/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "CerFeature.h"

@interface MKMapView (FeatureHighlight)

@property (strong) NSMutableArray *features;

- (void) addFeatureHighlight:(CerFeature*)feature;
- (void) removeFeatureHighlight:(CerFeature*)feature;

@end
