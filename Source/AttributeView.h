//
//  AttributeView.h
//  iWorld
//
//  Created by Jennifer Bieberstein on 3/04/13.
//
//

#import <UIKit/UIKit.h>

@interface AttributeView : UIView

- (void)showAttributes: (id<UITableViewDataSource>) dataSource;

@end

