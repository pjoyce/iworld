//
//  URLBuilder.m
//  iWorld
//
//  Created by Paul Joyce on 11/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "URLBuilder.h"

@implementation URLBuilder

+ (NSURL*)serverURLForConfigurationURL:(NSString*)relativeURL {
    // read configured server
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [standardUserDefaults stringForKey:@"server"];

    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/iworld/%@", server, relativeURL]];  //TODO: https

}

+ (NSURL*)localURLForConfigurationURL:(NSString*)relativeURL {

    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] objectAtIndex:0];
    return [documentsURL URLByAppendingPathComponent:relativeURL];
}

@end
