//
//  DownloadProgressTableViewCell.m
//  iWorld
//
//  Created by Paul Joyce on 30/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadProgressTableViewCell.h"

@implementation DownloadProgressTableViewCell

@synthesize label, progress;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
