//
//  AppConfigurationItem.h
//  iWorld
//
//  Created by Paul Joyce on 9/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConfigurationItem : NSObject

@property (strong) NSString *name;
@property (strong) NSString *availability;
@property (strong) NSString *type;
@property (strong) NSString *subtype;
@property (strong) NSString *url;
@property (strong) NSString *attribution;

@end
