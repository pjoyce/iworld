//
//  SearchLayersModel.m
//  iWorld
//
//  Created by Paul Joyce on 30/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchLayersModel.h"
#import "NSURL+Compare.h"
#import "AppConfiguration.h"
#import "AppConfigurationItem.h"
#import "URLBuilder.h"

@interface SearchLayersModel() {
@private
    NSMutableArray *array;
}
@end

@implementation SearchLayersModel

// singleton pattern
static SearchLayersModel *sharedInstance = nil;

+ (void)initialize
{
    if(!sharedInstance) {
        sharedInstance = [[SearchLayersModel alloc] init];
        sharedInstance->array = [NSMutableArray array];
    }
}

+ (SearchLayersModel *)sharedInstance
{
    return sharedInstance;
}

// load the model from the app configuration
- (void)reloadData {
    
    [array removeAllObjects];
    
    // setup the model
    NSArray *layers = [[AppConfiguration sharedInstance] searchLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
        
        SearchLayer *searchLayer = [[SearchLayer alloc] init];
        searchLayer.name    = layer.name;
        searchLayer.type    = layer.type;
        searchLayer.subtype = layer.subtype;
        
        if (layer.url) {
            if ([searchLayer.type isEqualToString:@"fts"]) {
                // the full source url
                searchLayer.url = [URLBuilder serverURLForConfigurationURL:layer.url];
                
                // was downloaded to this local file:// URL in the documents directory
                searchLayer.path = [[URLBuilder localURLForConfigurationURL:layer.url] path];

                // open the database
                searchLayer.fmdb = [FMDatabase databaseWithPath:searchLayer.path];
                [searchLayer.fmdb open];
                NSLog(@"opened database %@ %@",searchLayer.path, searchLayer.fmdb.goodConnection?@"YES":@"NO");
            }                
        }
        
        [array addObject:searchLayer];
    }
    
}

// nsarray passthroughs
- (NSUInteger)count {
    return array.count;
}

- (SearchLayer *)layerAtIndex:(NSUInteger)index {
    return [array objectAtIndex:index];
}

- (SearchLayer *)layerWithName:(NSString *)name {
    for (NSUInteger i=0; i<array.count; i++) {
        SearchLayer *searchLayer = [array objectAtIndex:i];
        if ([searchLayer.name isEqualToString:name]) {
            return searchLayer;
        }
    }
    return nil;
}

- (SearchLayer *)layerWithUrl:(NSURL *)url {
    for (NSUInteger i=0; i<array.count; i++) {
        SearchLayer *searchLayer = [array objectAtIndex:i];
        if ([searchLayer.url isEqualToURL:url]) {
            return searchLayer;
        }
    }
    return nil;
}

@end
