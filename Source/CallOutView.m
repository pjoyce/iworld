//
//  CallOutView.m
//  CallOutView
//
//  Created by Hendrik Holtmann on 18.01.10.
//  ported from MonoTouch

#import "CallOutView.h"

@implementation CallOutView

#define CENTER_IMAGE_WIDTH  31
#define CALLOUT_HEIGHT  70
#define MIN_LEFT_IMAGE_WIDTH  16
#define MIN_RIGHT_IMAGE_WIDTH  16
#define CENTER_IMAGE_ANCHOR_OFFSET_X  15
#define MIN_ANCHOR_X  (MIN_LEFT_IMAGE_WIDTH + CENTER_IMAGE_ANCHOR_OFFSET_X)
#define BUTTON_WIDTH  29
#define BUTTON_Y  10
#define TITLE_HEIGHT  30
#define TITLE_FONT_SIZE  17
#define SUBTITLE_HEIGHT 14
#define SUBTITLE_FONT_SIZE  11
#define ANCHOR_X  32
#define ANCHOR_Y  60


#define RECTFRAME CGRectMake (0, 0, 500, CALLOUT_HEIGHT)

- (id) initWithTitle:(NSString*)title subtitle:(NSString*)subtitle point:(CGPoint)point {
	if (self = [super initWithFrame:RECTFRAME]) {
        
        // move to anchor point
        self.frame = CGRectMake(point.x - ANCHOR_X, point.y - ANCHOR_Y, self.frame.size.width, self.frame.size.height);

        self.backgroundColor = [UIColor clearColor];
        self.opaque = false;

        // work out length of longest text
        UIFont *titleFont = [UIFont systemFontOfSize:TITLE_FONT_SIZE];
        UIFont *subtitleFont = [UIFont systemFontOfSize:SUBTITLE_FONT_SIZE];
        
        CGSize titleSize = [title sizeWithFont:titleFont];
        CGSize subtitleSize = [subtitle sizeWithFont:subtitleFont];
        CGFloat width = MAX(titleSize.width,subtitleSize.width);
        
        // set the frame based on text width +
        CGRect frame = self.frame;
        frame.size.width = width + MIN_LEFT_IMAGE_WIDTH + BUTTON_WIDTH + MIN_RIGHT_IMAGE_WIDTH + 3;
        frame.size.height = CALLOUT_HEIGHT;
        self.frame = frame;

        float left_width = ANCHOR_X - CENTER_IMAGE_ANCHOR_OFFSET_X;
        float right_width = self.frame.size.width - left_width - CENTER_IMAGE_WIDTH;
        
        calloutLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, left_width, CALLOUT_HEIGHT)];
        calloutLeft.image = [[UIImage imageNamed:@"callout_left.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:0];
        [self addSubview:calloutLeft];
        
        calloutCenter = [[UIImageView alloc] initWithFrame:CGRectMake(left_width, 0, CENTER_IMAGE_WIDTH, CALLOUT_HEIGHT)];
        calloutCenter.image = [UIImage imageNamed:@"callout_center.png"];
        [self addSubview:calloutCenter];
        
        calloutRight = [[UIImageView alloc] initWithFrame:CGRectMake(left_width + CENTER_IMAGE_WIDTH, 0, right_width, CALLOUT_HEIGHT)];
        calloutRight.image = [[UIImage imageNamed:@"callout_right.png"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
        [self addSubview:calloutRight];
        
        calloutTitle = [[UILabel alloc] initWithFrame:CGRectMake(MIN_LEFT_IMAGE_WIDTH, 6, titleSize.width, titleSize.height)];
        calloutTitle.text = title;
        calloutTitle.font = titleFont;
        calloutTitle.textColor = [UIColor whiteColor];
        calloutTitle.shadowColor = [UIColor blackColor];
        calloutTitle.shadowOffset = CGSizeMake(0, -1);
        calloutTitle.backgroundColor = [UIColor clearColor];
        [self addSubview:calloutTitle];
        
        calloutSubtitle = [[UILabel alloc] initWithFrame:CGRectMake(MIN_LEFT_IMAGE_WIDTH, TITLE_HEIGHT-2, subtitleSize.width, subtitleSize.height)];
        calloutSubtitle.text = subtitle;
        calloutSubtitle.font = subtitleFont;
        calloutSubtitle.textColor = [UIColor whiteColor];
        calloutSubtitle.shadowColor = [UIColor blackColor];
        calloutSubtitle.shadowOffset = CGSizeMake(0, -1);
        calloutSubtitle.backgroundColor = [UIColor clearColor];
        [self addSubview:calloutSubtitle];
        
        calloutButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        CGRect rect = calloutButton.frame;
        rect.origin.x = self.frame.size.width - BUTTON_WIDTH - MIN_RIGHT_IMAGE_WIDTH + 4;
        rect.origin.y = BUTTON_Y;
        calloutButton.frame = rect;
        [self addSubview:calloutButton];

        [self setUserInteractionEnabled:YES];
        [self animate];
	}
	return self;
}

-(void) animate {
	self.transform = CGAffineTransformMakeScale(0.8f, 0.8f);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationWillStartSelector:@selector(animationWillStart:context:)];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView setAnimationDuration:0.1f];
	self.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
	[UIView commitAnimations];
}

- (void)animationWillStart:(NSString *)animationID context:(void *)context {

}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	self.transform = CGAffineTransformIdentity;
}

- (void) addButtonTarget:(id)target action:(SEL)selector {
	[calloutButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}


@end
