//
//  MKMapView+FeatureHighlight.m
//  iWorld
//
//  Created by Paul Joyce on 24/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MKMapView+FeatureHighlight.h"

@implementation MKMapView (FeatureHighlight)

@synthesize features;

- (void) addFeatureHighlight:(CerFeature*)feature {
    @synchronized (features) {
        if (!features) {
            features = [NSMutableArray array];
        }
        [features addObject:feature];
    }
}

- (void) removeFeatureHighlight:(CerFeature*)feature {
    @synchronized (features) {
        if (features) {
            if ([features containsObject:feature]) {
                [features removeObject:feature];
            }
        }
    }
}

@end
