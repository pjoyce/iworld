//
//  URLBuilder.h
//  iWorld
//
//  Created by Paul Joyce on 11/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLBuilder : NSObject

+ (NSURL*)serverURLForConfigurationURL:(NSString*)relativeURL;
+ (NSURL*)localURLForConfigurationURL:(NSString*)relativeURL;

@end
