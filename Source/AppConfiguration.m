//
//  AppConfiguration.m
//  iWorld
//
//  Created by Paul Joyce on 9/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppConfiguration.h"
#import "AppConfigurationItem.h"

@interface AppConfiguration()
- (NSArray *)layersForKey:(NSString*)key;
@end

@implementation AppConfiguration

static AppConfiguration *sharedInstance = nil;

+ (void)initialize
{
    if(!sharedInstance) {
        sharedInstance = [[AppConfiguration alloc] init];
    }
}

+ (AppConfiguration *)sharedInstance
{
    return sharedInstance;
}


- (int) version {
    NSDictionary *config = [[NSUserDefaults standardUserDefaults] valueForKey:@"config"];
    return [[config valueForKey:@"version"] intValue];
}

- (CLLocationCoordinate2D)homeRegionSouthWest {
    NSDictionary *config = [[NSUserDefaults standardUserDefaults] valueForKey:@"config"];
    
    NSDictionary *home = [config valueForKey:@"home"];
    NSArray *visibleRegion = [home valueForKey:@"visible-region"];
    
    CLLocationCoordinate2D southWest;
    southWest.latitude  = [[visibleRegion objectAtIndex:0] doubleValue];
    southWest.longitude = [[visibleRegion objectAtIndex:1] doubleValue];
    return southWest;
}

- (CLLocationCoordinate2D)homeRegionNorthEast {
    NSDictionary *config = [[NSUserDefaults standardUserDefaults] valueForKey:@"config"];

    NSDictionary *home = [config valueForKey:@"home"];
    NSArray *visibleRegion = [home valueForKey:@"visible-region"];

    CLLocationCoordinate2D northEast;
    northEast.latitude  = [[visibleRegion objectAtIndex:2] doubleValue];
    northEast.longitude = [[visibleRegion objectAtIndex:3] doubleValue];
    return northEast;
}

- (NSArray *)homeLayers {
    NSDictionary *config = [[NSUserDefaults standardUserDefaults] valueForKey:@"config"];
    
    NSDictionary *home = [config valueForKey:@"home"];
    NSArray *visibleLayers = [home valueForKey:@"visible-layers"];
    return visibleLayers;
}

- (NSArray *)baseLayers {
    return [self layersForKey:@"base-layers"];
}

- (NSArray *)overlayLayers {
    return [self layersForKey:@"overlay-layers"];
}

- (NSArray *)searchLayers {
    return [self layersForKey:@"searches"];
}

- (NSArray *)layersForKey:(NSString*)key {
    NSDictionary *config = [[NSUserDefaults standardUserDefaults] valueForKey:@"config"];
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSArray *layers = [config valueForKey:key];
    for (NSUInteger i=0; i<layers.count; i++) {
        NSDictionary *item = [layers objectAtIndex:i];
        AppConfigurationItem *appConfigurationItem = [[AppConfigurationItem alloc] init];
        appConfigurationItem.name         = [item valueForKey:@"name"];
        appConfigurationItem.availability = [item valueForKey:@"availability"];
        appConfigurationItem.type         = [item valueForKey:@"type"];
        appConfigurationItem.subtype      = [item valueForKey:@"subtype"];
        appConfigurationItem.url          = [item valueForKey:@"url"];
        appConfigurationItem.attribution  = [item valueForKey:@"attribution"];
        [retval addObject:appConfigurationItem];
    }
    
    return retval;

}

@end
