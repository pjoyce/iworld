//
//  FileDownloader.m
//  iWorld
//
//  Created by Paul Joyce on 2/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileDownloader.h"

@interface FileDownloader() <NSURLConnectionDataDelegate> {
@private
    NSFileHandle *fileHandle;
    NSUInteger fileLength;
    NSUInteger writtenOut;
    BOOL success;
}

// NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

// NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection;
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;

/* does not work in iOS 5.0
// NSURLConnectionDownloadDelegate
- (void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes;
- (void)connectionDidResumeDownloading:(NSURLConnection *)connection totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes;
- (void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *) destinationURL;
*/

@end

@implementation FileDownloader

@synthesize source = _source;
@synthesize destination = _destination;
@synthesize delegate = _delegate;

-(void) downloadURL:(NSURL*)source toUrl:(NSURL*)destination delegate:(id)delegate {

    _source = source;
    _destination = destination;
    _delegate = delegate;
    
    // create output file
    NSString *destinationDir = [[destination path] stringByDeletingLastPathComponent];
    NSError *error;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if (![fileManager createDirectoryAtPath:destinationDir withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"error creating output dir %@",error);
        return;
    }
    
    [fileManager createFileAtPath:destination.path contents:nil attributes:nil];
    
    fileHandle = [NSFileHandle fileHandleForWritingToURL:destination error:&error];
    if (!fileHandle) {
        NSLog(@"error creating output file %@",error);
        return;        
    }
    
    NSURLRequest *fileRequest=[NSURLRequest requestWithURL:source cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    NSURLConnection *fileConnection=[NSURLConnection connectionWithRequest:fileRequest delegate:self];
    if (fileConnection) {
        // underway
        [self.delegate fileDownloader:self status:@"downloading"];
        [self.delegate fileDownloader:self progress:0];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    } else {
        
        // inform the delegate that the connection failed.
        [self.delegate fileDownloader:self status:@"connection to server failed"];
        
    }    

}

#pragma mark -  NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    fileLength = (NSInteger)response.expectedContentLength;
    writtenOut = 0;
    success = YES;
	if ([response respondsToSelector:@selector(statusCode)]) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode] != 200) {
            success = NO;
        }
    }
                       
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSLog(@"got %d bytes",data.length);
    [fileHandle writeData:data];
    writtenOut += data.length;
    [self.delegate fileDownloader:self progress:100.0*writtenOut/fileLength];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.delegate fileDownloader:self finished:success];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.delegate fileDownloader:self status:error.localizedDescription];
    [self.delegate fileDownloader:self finished:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
    return NO;
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
}

/*
 #pragma mark - NSURLConnectionDownloadDelegate
 - (void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes {
 
 }
 
 - (void)connectionDidResumeDownloading:(NSURLConnection *)connection totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes {
 
 }
 
 - (void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *) destinationURL {
 NSLog(@"downloaded to %@",destinationURL.absoluteString);
 
 NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
 NSLog(@"Documents: %@", documentsDirectory);
 
 NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] objectAtIndex:0];
 NSURL *finalDestinationURL = [documentsURL URLByAppendingPathComponent:@"foo.db"];
 NSLog(@"moving to %@", finalDestinationURL.absoluteString);
 
 NSError *error;
 if ([[NSFileManager defaultManager] moveItemAtURL:destinationURL toURL:finalDestinationURL error:&error] == NO) {
 NSLog(@"error moving file: %@",error.localizedDescription);
 } else {
 NSLog(@"ok!");
 }
 
 }
 */

@end
