//
//  MainViewController.h
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <RMMapView.h>

#import "BaseLayersViewController.h"
#import "OverlayLayersViewController.h"
#import "SearchSuggestionsViewController.h"
#import "SetupViewController.h"
#import "AttributeView.h"
#import "CerFeatureHighlight.h"

@interface MainViewController : UIViewController <UISearchBarDelegate, BaseLayerViewControllerDelegate, OverlayLayersViewControllerDelegate, SearchSuggestionsViewControllerDelegate, UIPopoverControllerDelegate, SetupViewControllerDelegate, MKMapViewDelegate, RMMapViewDelegate, UITableViewDelegate> {
    MKMapView *mkMapView;
    RMMapView *rmMapView;
    BaseLayersViewController *baseLayersViewController;
    OverlayLayersViewController *overlayLayersViewController;
    SearchSuggestionsViewController *searchSuggestionsViewController;
    
    CLLocationCoordinate2D mapRegionSouthWest;      // map region lower left
    CLLocationCoordinate2D mapRegionNorthEast;      // map region upper right
    BOOL regionHasChanged;                          // map region values are out of date (a zoom/pan has happened)
    
    UITapGestureRecognizer *tapGestureRecognizer;
    CerFeatureHighlight *featureHighlight;

}

@property (strong, nonatomic) UIPopoverController *popoverController;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet AttributeView *attributeView;

- (IBAction)homeButtonTouch:(id)sender;
- (IBAction)baseButtonTouch:(id)sender;
- (IBAction)overlaysButtonTouch:(id)sender;
- (IBAction)infoButtonTouch:(id)sender;

- (void)homeMap;

- (BOOL)showBaseMap:(NSString*)name;

- (BOOL)showOverlay:(NSString*)name;
- (BOOL)hideOverlay:(NSString*)name;

- (void)showMapKitMapWithSubType:(NSString*)subtype;
- (void)showRouteMeMapWithSubType:(NSString*)subtype andUrl:(NSURL*)url andPath:(NSString*)path;

- (void)storeRegion;
- (void)applyRegion;

// MKMapView callbacks
- (void)handleTapGesture:(UIGestureRecognizer *)gestureRecognizer;

// MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated;
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)ovl;
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views;
- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view;

// RMMapViewDelegate
- (RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation;
- (void) singleTapOnMap:(RMMapView *)map at:(CGPoint)point;

@end
