//
//  CerFeature.h
//  iWorld
//
//  Created by Paul Joyce on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CerGeometry.h"

@interface CerFeature : NSObject

@property (strong) NSString *featureid;
@property (strong) NSString *layer;
@property (strong) id <CerGeometry> geometry;
@property (strong) NSDictionary *attributes;

- (NSString*) description;

@end
