//
//  CerGeometry.h
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "RMMapView.h"

typedef struct {
	CLLocationCoordinate2D northEast;
	CLLocationCoordinate2D southWest;
} CerExtent;

@protocol CerGeometry

@required
@property (strong, readonly) NSString *type;
@property (        readonly) CerExtent extent;

- (BOOL) circaInteractsWithLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance;
- (CLLocationDegrees) distanceFromLocation:(CLLocationCoordinate2D)location;
- (id) initFromGeoJSON: (NSString*)json;
- (id<MKOverlay>) asMKOverlay;
- (RMMapLayer*) asRMMapLayerForMap:(RMMapView*)rmMapView;

@end
