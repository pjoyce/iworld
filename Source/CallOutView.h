//
//  CallOutView.h
//  CallOutView
//
//  Created by Hendrik Holtmann on 18.01.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CallOutView : UIView {

	UIImageView *calloutLeft;
	UIImageView *calloutCenter;
	UIImageView *calloutRight;
	UIButton *calloutButton;
	UILabel *calloutTitle;
	UILabel *calloutSubtitle;
	CGAffineTransform transform;
}

- (id) initWithTitle:(NSString*)title subtitle:(NSString*)subtitle point:(CGPoint)point;
- (void) addButtonTarget:(id)target action:(SEL)selector;

@end
