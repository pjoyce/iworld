//
//  NSURL+Compare.h
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Compare)

- (BOOL) isEqualToURL:(NSURL*)otherURL;

@end
