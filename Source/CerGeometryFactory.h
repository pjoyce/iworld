//
//  CerGeometryFactory.h
//  iWorld
//
//  Created by Lion User on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CerGeometry.h"

@interface CerGeometryFactory : NSObject

+ (CerGeometryFactory *)sharedInstance;

- (id<CerGeometry>) createGeometryFromGeoJSON:(NSString*)json;

@end
