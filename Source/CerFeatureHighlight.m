//
//  CerFeatureHighlight.m
//  iWorld
//
//  Created by Paul Joyce on 18/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CerFeatureHighlight.h"
#import <MapKit/MKOverlayView.h>
#import "RMMapView.h"
#import "RMAnnotation.h"
#import "RMMarker.h"
#import "RMShape.h"
#import "RMCircle.h"
#import "CallOutView.h"


@interface CerFeatureHighlight() {
@private
    CerFeature *feature;
}
@end

@implementation CerFeatureHighlight

@synthesize annotation, overlay, overlayView, rmAnnotation, rmAnnotation2, mkMapView, rmMapView;

- (id) initWithCerFeature:(CerFeature*)feature0 andMKMapView:(MKMapView*)mkMapView0 andRMMapView:(RMMapView*)rmMapView0 {
    if (self = [super init]) {
        feature = feature0;
        mkMapView = mkMapView0;
        rmMapView = rmMapView0;
        
        UIColor *highlightColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.6];
        CGFloat highlightLineWidth = 5.0;
        
        if (mkMapView) {
            annotation = [[CerFeatureAnnotation alloc] initWithCerFeature:feature];
            
            overlay = [feature.geometry asMKOverlay];
            
            if ([overlay isKindOfClass:[MKCircle class]]) {
                MKCircleView *circleView = [[MKCircleView alloc] initWithCircle:overlay];
                circleView.strokeColor = highlightColor;
                circleView.lineWidth = highlightLineWidth;
                overlayView = circleView;
            } else if ([overlay isKindOfClass:[MKPolyline class]]) {
                MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
                polylineView.strokeColor = highlightColor;
                polylineView.lineWidth = highlightLineWidth;
                overlayView = polylineView;
            } else if ([overlay isKindOfClass:[MKPolygon class]]) {
                MKPolygonView *polygonView = [[MKPolygonView alloc] initWithPolygon:overlay];
                polygonView.strokeColor = highlightColor;
                polygonView.lineWidth = highlightLineWidth;
                polygonView.fillColor = highlightColor;
                overlayView = polygonView;
            }
        }

        if (rmMapView) {
            annotation = [[CerFeatureAnnotation alloc] initWithCerFeature:feature];
            annotation.mapView = rmMapView;
            rmAnnotation  = [[RMAnnotation alloc] initWithMapView:rmMapView coordinate:annotation.coordinate andTitle:annotation.title];
            rmAnnotation2 = [[RMAnnotation alloc] initWithMapView:rmMapView coordinate:annotation.coordinate andTitle:annotation.title];

            // the marker label will follow the marker as the map is zoomed/panned, but the detail button does not work.
            // probably coordinate system related since the point is relative to the marker, not the mapview
            CallOutView *calloutView = [[CallOutView alloc] initWithTitle:annotation.title subtitle:annotation.subtitle point:CGPointMake(0,-20)];
            [calloutView addButtonTarget:self action:@selector(showFeatureDetails:)];
            RMMarker *rmMarker = [[RMMarker alloc] initWithUIImage:nil];
            rmMarker.label = calloutView;
            rmAnnotation.userInfo = rmMarker;

            // the "annotation view" can be a path/circle. saves having to use an overlay as well
            RMMapLayer *rmMapLayer = [feature.geometry asRMMapLayerForMap:rmMapView];
            
            if ([rmMapLayer isKindOfClass:[RMShape class]]) {
                RMShape *rmPath = (RMShape *)rmMapLayer;
                rmPath.lineColor = highlightColor;
                rmPath.lineWidth = highlightLineWidth;
                if ([feature.geometry.type isEqualToString:@"polygon"]) {
                    rmPath.fillColor = highlightColor;
                    //rmPath.drawingMode = kCGPathFillStroke;
                } else {
                    //rmPath.drawingMode = kCGPathStroke;
                }
            } else if ([rmMapLayer isKindOfClass:[RMCircle class]]) {
                RMCircle *rmCircle = (RMCircle *)rmMapLayer;
                rmCircle.lineColor = highlightColor;
                rmCircle.lineWidthInPixels = highlightLineWidth;
                rmCircle.fillColor = [UIColor clearColor];
            }
            rmAnnotation2.userInfo = rmMapLayer;
        }
        
    }
    return self;
}

- (void) show {
    
    // add annotation/overlay from feature highlight to map
    if (mkMapView.hidden == NO) {
        [mkMapView addOverlay:overlay];
    } else if (rmMapView.hidden == NO) {
        [rmMapView addAnnotation:rmAnnotation2];    // add the shape... 
        [rmMapView addAnnotation:rmAnnotation];     // ... then the label, so the label is on top
    }
}

- (void) hide {
    
    // remove any existing annotation/overlay from feature highlight
    if (mkMapView.hidden == NO) {
        [mkMapView removeOverlay:overlay];
    } else if (rmMapView.hidden == NO) {
        [rmMapView removeAnnotation:rmAnnotation];
        [rmMapView removeAnnotation:rmAnnotation2];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"numRowsInSection called");
    // count the number of attributes that don't start with myw_
    CerFeatureAnnotation *featureAnnotation = (CerFeatureAnnotation *)self.annotation;
    NSUInteger count = 0;
    for (NSString *key in featureAnnotation.attributes.allKeys) {
        if (![key hasPrefix:@"myw_"] && ![key isEqualToString:@"relationships"]) {
            count++;
        }
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Attribute Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.SelectionStyle = UITableViewCellSelectionStyleNone;
        UIFont *font = [UIFont systemFontOfSize:15.0];
        cell.textLabel.font = font;
        cell.detailTextLabel.font = font;
    }
    
    // find the attribute at position indexPath.row, skipping myw_ ones
    CerFeatureAnnotation *featureAnnotation = (CerFeatureAnnotation *)self.annotation;
    NSUInteger count = 0;
    for (NSString *key in featureAnnotation.attributes.allKeys) {
        if (![key hasPrefix:@"myw_"] && ![key isEqualToString:@"relationships"]) {
            if (count == indexPath.row) {
                // atrribute name
                NSString *attributeName = [[key stringByReplacingOccurrencesOfString:@"_" withString:@" "] capitalizedString];
                cell.textLabel.text = attributeName;
                
                // attribute value
                NSObject *val = [featureAnnotation.attributes valueForKey:key];
                
                NSString *attributeValue;
                if ([val isKindOfClass:[NSString class]]) {
                    attributeValue = (NSString*)val;
                } else if ([val isKindOfClass:[NSNumber class]]) {
                    attributeValue = [(NSNumber*)val stringValue];
                } else {
                    attributeValue = [val description];
                }
                cell.detailTextLabel.text = attributeValue;
                
                break;
            }
            count++;
        }
    }
    
    
    return cell;
}


@end
