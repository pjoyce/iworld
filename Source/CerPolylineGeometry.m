//
//  CerPolylineGeometry.m
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CerPolylineGeometry.h"
#import "CerPointGeometry.h"
#import "RMMapView.h"
#import "RMShape.h"

@implementation CerPolylineGeometry

@synthesize path;

-(NSString*) type {
    return @"polyline";
}

-(CerExtent) extent {
	CerExtent e;
    e.northEast.latitude = -HUGE_VAL;
    e.northEast.longitude = -HUGE_VAL;
    e.southWest.latitude = HUGE_VAL;
    e.southWest.longitude = HUGE_VAL;
	for (CerPointGeometry *vertex in path) {
		e.northEast.latitude = MAX(e.northEast.latitude, vertex.latitude);
		e.northEast.longitude = MAX(e.northEast.longitude, vertex.longitude);
		e.southWest.latitude = MIN(e.southWest.latitude, vertex.latitude);
		e.southWest.longitude = MIN(e.southWest.longitude, vertex.longitude);
	}
    return e;
}

-(BOOL) circaInteractsWithLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance {
    double distanceToLocation = HUGE_VAL;
    for (NSUInteger i=0; i<path.count-1; i++) {
        CerPointGeometry *A = (CerPointGeometry *)[path objectAtIndex:i];
        CerPointGeometry *B = (CerPointGeometry *)[path objectAtIndex:i+1];
        
        double normalLength = sqrt((B.longitude - A.longitude) * (B.longitude - A.longitude) + (B.latitude - A.latitude) * (B.latitude - A.latitude));
        double distanceToAB = fabs((location.longitude - A.longitude) * (B.latitude - A.latitude) - (location.latitude - A.latitude) * (B.longitude - A.longitude)) / normalLength;
        distanceToLocation = MIN(distanceToAB, distanceToLocation);
    }
    if (distanceToLocation <= distance) {
        return YES;
    } else {
        return NO;
    }
}

- (CLLocationDegrees) distanceFromLocation:(CLLocationCoordinate2D)location {
    double distanceToLocation = HUGE_VAL;
    for (NSUInteger i=0; i<path.count-1; i++) {
        CerPointGeometry *A = (CerPointGeometry *)[path objectAtIndex:i];
        CerPointGeometry *B = (CerPointGeometry *)[path objectAtIndex:i+1];
        double vx = A.longitude - location.longitude;
        double vy = A.latitude - location.latitude;
        double ux = B.longitude - A.longitude;
        double uy = B.latitude - A.latitude;
        double length = ux*ux + uy*uy;
        
        double distanceToAB;
        
        // from http://forums.codeguru.com/showpost.php?p=1913101&postcount=16
        double det = (-vx * ux) + (-vy * uy); //if this is < 0 or > length then its outside the line segment
        if (det < 0) {
            distanceToAB = sqrt((A.longitude - location.longitude) * (A.longitude - location.longitude) + (A.latitude - location.latitude) * (A.latitude - location.latitude));
        } else if (det > length) {
            distanceToAB = sqrt((B.longitude - location.longitude) * (B.longitude - location.longitude) + (B.latitude - location.latitude) * (B.latitude - location.latitude));
        } else {
            det = ux * vy - uy * vx;
            distanceToAB = sqrt((det * det) / length);
        }
        distanceToLocation = MIN(distanceToAB, distanceToLocation);
    }
    return distanceToLocation;
}

- (id) initFromGeoJSON:(NSString*)json {
    if (self = [super init]) {

		// {"type": "LineString", "coordinates": [[-76.164807973702281, 42.601725025385967], [-76.164816507697466, 42.601829114136443], [-76.164816507697466, 42.601829114136443], [-76.164831150236608, 42.602007084085997], [-76.164850913172856, 42.602248965005167], [-76.164842828335296, 42.602390551764749]]}
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        NSDictionary *dict = [NSJSONSerialization  JSONObjectWithData:jsonData options:0 error:&error];
        if (!error) {
            
            NSString *type = [dict valueForKey:@"type"];
            if ([[type lowercaseString] isEqualToString:@"linestring"]) {
                
				NSMutableArray *mutablePath = [NSMutableArray array];
                NSArray *coords = [dict valueForKey:@"coordinates"];
				for (NSArray *coord in coords) {
					if (coord.count == 2) {
						CLLocationCoordinate2D location;
						location.longitude = [[coord objectAtIndex:0] doubleValue];
						location.latitude  = [[coord objectAtIndex:1] doubleValue];
						CerPointGeometry *point = [[CerPointGeometry alloc] initWithLocation:location];
						[mutablePath addObject:point];
					}
				}
				path = mutablePath;
            }
        } else {
            NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        }
	}
    return self;
    
}

- (id<MKOverlay>) asMKOverlay {
    NSMutableData* data = [NSMutableData dataWithLength:sizeof(CLLocationCoordinate2D) * path.count];
    CLLocationCoordinate2D* array = [data mutableBytes];

	for (NSUInteger i=0; i<path.count; i++) {
        CerPointGeometry *vertex = (CerPointGeometry *)[path objectAtIndex:i];
        array[i].latitude = vertex.latitude;
        array[i].longitude = vertex.longitude;
    }

    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:array count:path.count];
    return polyline;
}

- (RMMapLayer*) asRMMapLayerForMap:(RMMapView*)rmMapView {
    RMShape *rmpath = [[RMShape alloc] initWithView:rmMapView];
    
    CLLocationCoordinate2D coordinate;

	for (NSUInteger i=0; i<path.count; i++) {
        CerPointGeometry *vertex = (CerPointGeometry *)[path objectAtIndex:i];
        coordinate.latitude = vertex.latitude;
        coordinate.longitude = vertex.longitude;
        [rmpath addLineToCoordinate:coordinate];
    }
    return rmpath;
}

@end
