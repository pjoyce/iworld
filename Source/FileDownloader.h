//
//  FileDownloader.h
//  iWorld
//
//  Created by Paul Joyce on 2/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FileDownloaderDelegate <NSObject>

-(void) fileDownloader:(id)sender status:(NSString*)message;
-(void) fileDownloader:(id)sender progress:(NSInteger)percent;
-(void) fileDownloader:(id)sender finished:(BOOL)completedOk;

@end

@interface FileDownloader : NSObject 

@property (strong, readonly) NSURL *source;
@property (strong, readonly) NSURL *destination;
@property (strong, readonly) id delegate;

-(void) downloadURL:(NSURL*)source toUrl:(NSURL*)destination delegate:(id)delegate;

@end
