//
//  SetupViewController.m
//  iWorld
//
//  Created by Paul Joyce on 5/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SetupViewController.h"
#import "AppConfiguration.h"
#import "AppConfigurationItem.h"
#import "URLBuilder.h"
#import "FileDownloader.h"
#import "CredentialsTableViewCell.h"
#import "DownloadProgressTableViewCell.h"

@interface SetupViewController() <FileDownloaderDelegate> {
    FileDownloader *configDownloader;
    NSUInteger downloadingCount;
    NSMutableArray *downloadingList;
    NSMutableDictionary *downloadingDict;
}

// FileDownloaderDelegate
-(void) fileDownloader:(id)sender progress:(NSInteger)percent;
-(void) fileDownloader:(id)sender status:(NSString*)message;
-(void) fileDownloader:(id)sender finished:(BOOL)completedOk;

// set the file attribute that stops it getting backed to iCloud
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end

NSString *CONFIG = @"config";

@implementation SetupViewController

@synthesize navigationBar;
@synthesize credentialsTableView;
@synthesize connectButton;
@synthesize statusLabel;
@synthesize downloadsTableView;
@synthesize cancelButton;
@synthesize delegate;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    credentialsTableView.dataSource = self;
    downloadsTableView.dataSource = self;

    credentialsTableView.hidden = NO;
    connectButton.hidden = NO;
    statusLabel.hidden = NO;
    [statusLabel setText:@""];
    downloadsTableView.hidden = YES;

    // default to my ec2 server for now
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *server   = [standardUserDefaults stringForKey:@"server"];
    if (server.length == 0) {
        server = @"ec2-50-16-87-96.compute-1.amazonaws.com";
        [standardUserDefaults setObject:server forKey:@"server"];
        [standardUserDefaults synchronize];
    }

}

- (void)viewDidUnload {
    [self setStatusLabel:nil];
    [self setCredentialsTableView:nil];
    [self setDownloadsTableView:nil];
    [self setConnectButton:nil];
    [self setNavigationBar:nil];
    [self setCancelButton:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (IBAction)cancelButtonTap:(id)sender {
    [delegate setupViewControllerDidCancel:self];
}

// http://stackoverflow.com/questions/3372333/ipad-keyboard-will-not-dismiss-if-modal-view-controller-presentation-style-is-ui
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (IBAction)connectToServer:(id)sender {
    // need to download configuration and data
    [statusLabel setText:@"connecting to server..."];
    
    UITextField *textField0 = [(CredentialsTableViewCell*)[credentialsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] textField];
    UITextField *textField1 = [(CredentialsTableViewCell*)[credentialsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]] textField];
    UITextField *textField2 = [(CredentialsTableViewCell*)[credentialsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]] textField];

    [textField0 resignFirstResponder];
    [textField1 resignFirstResponder];
    [textField2 resignFirstResponder];

	NSString *server   = [textField0 text];
	NSString *username = [textField1 text];
	NSString *password = [textField2 text];

    // update the values set for next time around
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:server   forKey:@"server"];
    [standardUserDefaults setObject:username forKey:@"username"];
    [standardUserDefaults setObject:password forKey:@"password"];
    [standardUserDefaults synchronize];

    [self downloadConfigurationFromServer];
}

- (void)downloadConfigurationFromServer {
    
    // download settings_server/iworld/config
    NSURL *configURL = [URLBuilder serverURLForConfigurationURL:CONFIG];
    
    // to this local file:// URL
    NSURL *destinationURL = [URLBuilder localURLForConfigurationURL:CONFIG];

    configDownloader = [[FileDownloader alloc] init];
    [configDownloader downloadURL:configURL toUrl:destinationURL delegate:self];
}

// this is invoked once configDownloader completes
// parse the config
- (void)loadDownloadedConfiguration {

    // it was downloaded to this destination
    NSURL *configFileURL = [URLBuilder localURLForConfigurationURL:CONFIG];

    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:configFileURL options:0 error:&error];
    if (error) {
        NSLog(@"[%@ %@] config file reading error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        [statusLabel setText:error.localizedDescription];
        return;
    }

    NSDictionary *config = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (error) {
        NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        [statusLabel setText:error.localizedDescription];
        return;
    }
    
    int version = [[config valueForKey:@"version"] intValue];
    if (version == 0) {
        NSLog(@"config not read properly");
        [statusLabel setText:@"downloaded configuration is invalid; see server administrator"];
        return;
    }
    
    // stash in user defaults
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:config forKey:@"config"];
    [standardUserDefaults synchronize];
}

- (void)downloadDataFromServer {
    downloadingCount = 0;
    downloadingList = [NSMutableArray array];
    downloadingDict = [NSMutableDictionary dictionary];
    
    NSArray *layers = nil;
    
    //   base layers
    layers = [[AppConfiguration sharedInstance] baseLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
        
        if (layer.url) {
            if (([layer.subtype isEqualToString:@"mbtiles"] || [layer.subtype isEqualToString:@"mbtiles++"])) {
                // download this url
                NSURL *downloadURL = [URLBuilder serverURLForConfigurationURL:layer.url];
                
                // to this local file:// URL
                NSURL *destinationURL = [URLBuilder localURLForConfigurationURL:layer.url];

                // the delegate (self) is a NSURLConnectionDataDelegate
                FileDownloader *downloader = [[FileDownloader alloc] init];
                [downloader downloadURL:downloadURL toUrl:destinationURL delegate:self];
                [downloadingList addObject:downloadURL];
                [downloadingDict setObject:layer.name forKey:downloadURL];
                downloadingCount++;
            }
        }
    }

    //   overlay layers
    layers = [[AppConfiguration sharedInstance] overlayLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
        
        if (layer.url) {
            if (([layer.subtype isEqualToString:@"mbtiles"] || [layer.subtype isEqualToString:@"mbtiles++"])) {
                // download this url
                NSURL *downloadURL = [URLBuilder serverURLForConfigurationURL:layer.url];
                
                // to this local file:// URL
                NSURL *destinationURL = [URLBuilder localURLForConfigurationURL:layer.url];
                
                // the delegate (self) is a NSURLConnectionDataDelegate
                FileDownloader *downloader = [[FileDownloader alloc] init];
                [downloader downloadURL:downloadURL toUrl:destinationURL delegate:self];
                [downloadingList addObject:downloadURL];
                [downloadingDict setObject:layer.name forKey:downloadURL];
                downloadingCount++;
            }
        }
    }

    //   search layers
    layers = [[AppConfiguration sharedInstance] searchLayers];
    for (NSUInteger i=0; i<layers.count; i++) {
        AppConfigurationItem *layer = [layers objectAtIndex:i];
        
        if (layer.url) {
            if ([layer.type isEqualToString:@"fts"]) {
                // download this url
                NSURL *downloadURL = [URLBuilder serverURLForConfigurationURL:layer.url];
                
                // to this local file:// URL
                NSURL *destinationURL = [URLBuilder localURLForConfigurationURL:layer.url];
                
                // the delegate (self) is a NSURLConnectionDataDelegate
                FileDownloader *downloader = [[FileDownloader alloc] init];
                [downloader downloadURL:downloadURL toUrl:destinationURL delegate:self];
                [downloadingList addObject:downloadURL];
                [downloadingDict setObject:layer.name forKey:downloadURL];
                downloadingCount++;
            }
        }
    }

    // anything to download?
    if (downloadingCount == 0) {
        // nothing to download, proceed straight away
        [delegate setupViewControllerDidFinish:self];
    } else {
        // activate the progress views
        downloadsTableView.hidden = NO;
        [downloadsTableView reloadData];
        
        // the download completion callbacks will proceed once all finished
        NSLog(@"waiting for downloads");
    }
}

#pragma mark - FileDownloaderDelegate

-(void) fileDownloader:(id)sender progress:(NSInteger)percent {
    NSLog(@"progress %@ %d",[sender source],percent);

    NSUInteger row = [downloadingList indexOfObject:[sender source]];
    DownloadProgressTableViewCell *downloadProgressTableViewCell = (DownloadProgressTableViewCell *)[downloadsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    // big files return -ve percent towards the end
    if (percent > 0) {
        [downloadProgressTableViewCell.progress setProgress:percent/100.0];
    } else {
        //would be good to show indeterminate, but UIPRogressView doesn't
    }
}

-(void) fileDownloader:(id)sender status:(NSString*)message {
    [statusLabel setText:message];
    NSLog(@"status %@ %@",[sender source],message);
}

-(void) fileDownloader:(id)sender finished:(BOOL)completedOk {
    NSLog(@"finished %@ %@",[sender source], completedOk?@"YES":@"NO");

    // check if its the config or some data
    if (sender == configDownloader) {
        // config
        if (completedOk) {
            [self loadDownloadedConfiguration];
            [self downloadDataFromServer];
        } else {
            [statusLabel setText:@"configuration download failed"];
            [delegate setupViewControllerDidCancel:self];
        }
        
    } else {
        // data
        
        // stop this file going to iCloud
        [self addSkipBackupAttributeToItemAtURL:[sender destination]];
        
        downloadingCount--;
        // if we are all done downloading, return to the main view controller
        if (downloadingCount == 0) {
            [delegate setupViewControllerDidFinish:self];
        }
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    
    NSNumber *backupValue = [NSNumber numberWithBool:NO];
    NSError *error;
    [URL setResourceValue:backupValue forKey:NSURLIsExcludedFromBackupKey error:&error];
    if (error) {NSLog(@"error setting backup attribute: %@",error);}
    return !error;
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // both credentials and download progress have one section
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == credentialsTableView) {
        // top section for credentials (server,username,password)
        return @"Server credentials";
    } else {
        // bottom section for download progress
        return @"Data download progress";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == credentialsTableView) {
        // top section for credentials (server,username,password)
        return 3;
    } else {
        // bottom section for download progress
        return downloadingCount;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // get the right kind of cell, based on the tableview it is for
    NSString *cellIdentifier;
    if (tableView == credentialsTableView) {
        cellIdentifier = @"credentials cell";
    } else {
        cellIdentifier = @"download progress cell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // populate the cell
    if (tableView == credentialsTableView) {
        CredentialsTableViewCell *credentialsTableViewCell = (CredentialsTableViewCell*)cell;

        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *server   = [standardUserDefaults stringForKey:@"server"];
        NSString *username = [standardUserDefaults stringForKey:@"username"];
        NSString *password = [standardUserDefaults stringForKey:@"password"];
        
        switch (indexPath.row) {
            case 0:
                credentialsTableViewCell.label.text = @"Server";
                credentialsTableViewCell.textField.secureTextEntry = NO;
                credentialsTableViewCell.textField.text = server;
                break;
            case 1:
                credentialsTableViewCell.label.text = @"Username";
                credentialsTableViewCell.textField.secureTextEntry = NO;
                credentialsTableViewCell.textField.text = username;
                break;
            case 2:
                credentialsTableViewCell.label.text = @"Password";
                credentialsTableViewCell.textField.secureTextEntry = YES;
                credentialsTableViewCell.textField.text = password;
                break;
        }
    } else {
        DownloadProgressTableViewCell *downloadProgressTableViewCell = (DownloadProgressTableViewCell*)cell;
        
        [downloadProgressTableViewCell.label setText:[downloadingDict objectForKey:[downloadingList objectAtIndex:indexPath.row]]];
        [downloadProgressTableViewCell.progress setProgress:0];
    }
    
    return cell;
}

@end
