//
//  CerFeature.m
//  iWorld
//
//  Created by Paul Joyce on 11/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CerFeature.h"

@implementation CerFeature

@synthesize featureid, layer, geometry, attributes;

- (NSString*) description {
    return [NSString stringWithFormat:@"%@ %@ %@",featureid,layer,geometry.type];
}

@end
