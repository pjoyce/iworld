//
//  CerPointGeometry.m
//  iWorld
//
//  Created by Paul Joyce on 31/10/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CerPointGeometry.h"
#import "RMMapView.h"
#import "RMCircle.h"

@implementation CerPointGeometry

@synthesize latitude,longitude;

- (NSString*) type {
    return @"point";
}

- (CerExtent) extent {
	CerExtent e;
	e.northEast.latitude = latitude;
	e.northEast.longitude = longitude;
	e.southWest.latitude = latitude;
	e.southWest.longitude = longitude;
    return e;
}

- (BOOL) circaInteractsWithLocation:(CLLocationCoordinate2D)location withinDistance:(CLLocationDegrees)distance {
    CLLocationDegrees dx = location.longitude - longitude;
    CLLocationDegrees dy = location.latitude - latitude;
    // compare squared distances
    if ((dx*dx + dy*dy) <= distance*distance) {
        return YES;
    } else {
        return NO;
    }
}

- (CLLocationDegrees) distanceFromLocation:(CLLocationCoordinate2D)location {
    CLLocationDegrees dx = location.longitude - longitude;
    CLLocationDegrees dy = location.latitude - latitude;
    return sqrt(dx*dx + dy*dy);
}

- (id) initFromGeoJSON:(NSString*)json {
    if (self = [super init]) {
        longitude = 0.0;
        latitude = 0.0;

        // {"type":"Point","coordinates":[-76.17616860777443,42.59962142858127]}
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        NSDictionary *dict = [NSJSONSerialization  JSONObjectWithData:jsonData options:0 error:&error];
        if (!error) {
            
            NSString *type = [dict valueForKey:@"type"];
            if ([[type lowercaseString] isEqualToString:@"point"]) {
                
                NSArray *coords = [dict valueForKey:@"coordinates"];
                if (coords.count == 2) {
                    longitude = [[coords objectAtIndex:0] doubleValue];
                    latitude  = [[coords objectAtIndex:1] doubleValue];
                }
            }
        } else {
            NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        }
    }
    return self;

}

- (id) initWithLocation:(CLLocationCoordinate2D)location {
    if (self = [super init]) {
        longitude = location.longitude;
        latitude = location.latitude;
	}
	return self;
}

- (id<MKOverlay>) asMKOverlay {
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    
    CLLocationDistance radius = 10; // meters
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:coordinate radius:radius];
    return circle;
}

- (RMMapLayer*) asRMMapLayerForMap:(RMMapView*)rmMapView {
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    
    CLLocationDistance radius = 10; // meters

    RMCircle *circle = [[RMCircle alloc] initWithView:rmMapView radiusInMeters:radius];
    return circle;
    
    /*
    CLLocationCoordinate2D coordinate;

    CLLocationDistance radius = 10.0 * 360.0 / (2.0*M_PI*6400000.0); // 10 meters * 360 degrees / meters around equator

    RMPath *rmpath = [[RMPath alloc] initForMap:rmMapView];
    
    for (int i=0; i<10; i++) {
        double theta = 2.0*M_PI/10.0*(double)i;
        coordinate.latitude = latitude + radius * sin(theta);
        coordinate.longitude = longitude + radius * cos(theta);
        [rmpath addLineToLatLong:coordinate];
    }
    [rmpath closePath];
    return rmpath;
     */
}

@end
