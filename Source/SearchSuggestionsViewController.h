//
//  SearchSuggestionsViewController.h
//  iWorld
//
//  Created by Paul Joyce on 26/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SearchSuggestionsViewController;

@protocol SearchSuggestionsViewControllerDelegate
// Sent when the user selects a row in the search suggestions list.
- (void)searchSuggestionsViewController:(SearchSuggestionsViewController *)controller didSelectString:(NSString *)searchString;
@end

@interface SearchSuggestionsViewController : UITableViewController

@property (weak, nonatomic) IBOutlet id <SearchSuggestionsViewControllerDelegate> delegate;

@end
