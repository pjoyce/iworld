//
//  OverlayLayer.h
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import <MapKit/MapKit.h>
#import <RMTileSource.h>

@interface OverlayLayer : NSObject

@property (strong) NSString *name;              // name of layer
@property (strong) NSString *type;              // type of layer
@property (strong) NSString *subtype;           // subtype of layer
@property          BOOL visible;                // currently visible or not
@property (strong) NSURL *url;                  // http: or https: url to resource
@property (strong) NSString *path;              // file: path to locally downloaded url if offline
@property (strong) FMDatabase *fmdb;            // handle to FMDatabase if offline
@property (strong) id<MKOverlay> mkOverlay;     // overlay added to Map Kit map
@property (strong) id<RMTileSource> rmOverlay;  // overlay added to Route-Me map

@end
