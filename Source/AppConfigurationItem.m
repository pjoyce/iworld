//
//  AppConfigurationItem.m
//  iWorld
//
//  Created by Paul Joyce on 9/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppConfigurationItem.h"

@implementation AppConfigurationItem

@synthesize name;
@synthesize availability;
@synthesize type;
@synthesize subtype;
@synthesize url;
@synthesize attribution;

@end
