//
//  AttributeView.m
//  iWorld
//
//  Created by Jennifer Bieberstein on 3/04/13.
//
//

#import "AttributeView.h"

@implementation AttributeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)showAttributes: (id <UITableViewDataSource>) dataSource {
    // get the tableview (there is probably a better way to do this)
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UITableView class]]) {
            UITableView *tableView = (UITableView *)view;
            // set the data source so that the info is read from the
            // CerFeatureHighlight
            tableView.dataSource = dataSource;
            [tableView reloadData];
        }
    }
}

@end
