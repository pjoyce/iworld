//
//  SetupViewController.h
//  iWorld
//
//  Created by Paul Joyce on 5/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SetupViewController;

@protocol SetupViewControllerDelegate
@optional
- (void)setupViewControllerDidFinish:(SetupViewController*)viewController;
- (void)setupViewControllerDidCancel:(SetupViewController*)viewController;
@end

@interface SetupViewController : UIViewController <UITableViewDataSource> 

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *cancelButton;
@property (weak, nonatomic) IBOutlet UITableView *credentialsTableView;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UITableView *downloadsTableView;

@property (weak, nonatomic) IBOutlet id <SetupViewControllerDelegate> delegate;

- (IBAction)connectToServer:(id)sender;

@end
