//
//  OverlayLayersViewController.h
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OverlayLayersViewController;

@protocol OverlayLayersViewControllerDelegate
- (void)overlayLayersViewController:(OverlayLayersViewController *)controller didSelectLayer:(NSString*)layerName;
- (void)overlayLayersViewController:(OverlayLayersViewController *)controller didUnselectLayer:(NSString*)layerName;
@end

@interface OverlayLayersViewController : UITableViewController

@property (weak, nonatomic) IBOutlet id <OverlayLayersViewControllerDelegate> delegate;

@end
