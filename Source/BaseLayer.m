//
//  BaseLayer.m
//  iWorld
//
//  Created by Paul Joyce on 3/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseLayer.h"

@implementation BaseLayer

@synthesize name, type, subtype, url, path, visible;

@end
