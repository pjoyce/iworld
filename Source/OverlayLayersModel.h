//
//  OverlayLayersModel.h
//  iWorld
//
//  Created by Paul Joyce on 1/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverlayLayer.h"

@interface OverlayLayersModel : NSObject

+ (OverlayLayersModel *)sharedInstance;

- (void)reloadData;

- (NSUInteger)count;
- (OverlayLayer *)layerAtIndex:(NSUInteger)index;

- (OverlayLayer *)layerWithName:(NSString *)name;
- (OverlayLayer *)layerWithUrl:(NSURL *)url;

@end
