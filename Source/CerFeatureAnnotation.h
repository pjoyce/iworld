//
//  CerFeatureAnnotation.h
//  iWorld
//
//  Created by Paul Joyce on 12/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "RMAnnotation.h"
#import "CerFeature.h"

@interface CerFeatureAnnotation : RMAnnotation <MKAnnotation>

@property (strong) NSString *featureid;
//@property (strong) NSString *layer;
@property (strong) id <CerGeometry> geometry;
@property (strong) NSDictionary *attributes;

- (id) initWithCerFeature:(CerFeature*)feature;


// MKAnnotation
//@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
//@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

@end
