//
//  MainViewController.m
//  iWorld
//
//  Created by Paul Joyce on 8/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"

#import <RMOpenStreetMapSource.h>
#import <RMMapQuestOSMSource.h>
#import "RMMBTilesTileSource.h"

#import "MBTilesOverlay.h"
#import "MBTilesOverlayView.h"

#import "AppConfiguration.h"
#import "SetupViewController.h"
#import "BaseLayer.h"
#import "BaseLayersModel.h"
#import "OverlayLayer.h"
#import "OverlayLayersModel.h"
#import "SearchLayer.h"
#import "SearchLayersModel.h"
#import "SpatialSearch.h"
#import "SearchSuggestionsModel.h"
#import "CerFeatureAnnotation.h"
#import "CerGeometry.h"
#import "AttributeView.h"

@implementation MainViewController

@synthesize popoverController, attributeView;
@synthesize searchBar;

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    // viewcontroller for list of base layers
    baseLayersViewController = [[BaseLayersViewController alloc] init];
    baseLayersViewController.delegate = self;

    // viewcontroller for list of overlay layers
    overlayLayersViewController = [[OverlayLayersViewController alloc] init];
    overlayLayersViewController.delegate = self;

    // viewcontroller for list of search suggestions
    searchSuggestionsViewController = [[SearchSuggestionsViewController alloc] initWithStyle:UITableViewStylePlain];
    searchSuggestionsViewController.delegate = self;
    
    // one popover controller to manage the above three
    popoverController = [[UIPopoverController alloc] initWithContentViewController:baseLayersViewController];
    popoverController.delegate = self;
    
    // create Map Kit view
    mkMapView = [[MKMapView alloc] initWithFrame:self.view.frame];
    mkMapView.delegate = self;
    mkMapView.hidden = YES;
    [self.view addSubview:mkMapView];
    
    // create Route-Me view
    rmMapView = [[RMMapView alloc] initWithFrame:self.view.frame];
    rmMapView.delegate = self;
    rmMapView.hidden = YES;
    [self.view addSubview:rmMapView];

    // initialize the mapview frames by pretending we have just rotated (the arg is ignored)
    [self didRotateFromInterfaceOrientation:UIInterfaceOrientationPortrait];
    
    // capture taps on the MapKit map
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGestureRecognizer.enabled = YES;
    [mkMapView addGestureRecognizer:tapGestureRecognizer];

    // show the blue dot for user location
    mkMapView.showsUserLocation = YES;
    rmMapView.showsUserLocation = YES;

    // nothing highlighted yet
    featureHighlight = nil;
    
}

- (void)viewDidUnload {
    attributeView = nil;
    attributeView = nil;
    attributeView = nil;
    attributeView = nil;
    [self setAttributeView:nil];
    [super viewDidUnload];
    [self setSearchBar:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    // check configuration state
    AppConfiguration *appConfig = [AppConfiguration sharedInstance];
    if (appConfig.version != 0) {
        // configured ok - safe to complete now
        [self completeTheInitialization];
    } else {
        // unconfigured - prompt for server details
        SetupViewController *setupViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupVC"];
        setupViewController.delegate = self;
        [self presentModalViewController:setupViewController animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    // TODO: save state here
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES; all orientations supported
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    // work out how much room for the map view we have by subtracting toolbar from total
    CGRect mapViewRect = self.view.bounds;
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UIToolbar class]]) {
            mapViewRect.origin.y += view.bounds.size.height;
            mapViewRect.size.height -= view.bounds.size.height;
        }
    }
    
    // adjust the map views
    [mkMapView setFrame:mapViewRect];
    [rmMapView setFrame:mapViewRect];
    
}

- (void) completeTheInitialization {

    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:i];
        if (overlayLayer.rmOverlay) {
            [rmMapView removeTileSource:overlayLayer.rmOverlay];
        }
    }
    
    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    [baseLayersModel reloadData];
    
    //OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    [overlayLayersModel reloadData];
    
    SearchLayersModel *searchLayersModel = [SearchLayersModel sharedInstance];
    [searchLayersModel reloadData];

    // display the initial map
    [self homeMap];

}

#pragma mark - Setup View Controller Delegate

- (void)setupViewControllerDidFinish:(SetupViewController*)viewController {
    [self completeTheInitialization];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)setupViewControllerDidCancel:(SetupViewController*)viewController {
    // do nothing except hide the modal form jbi: (form for connecting to server)
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Base Layer View Controller Delegate

- (void)baseLayerViewController:(BaseLayersViewController *)controller didSelectLayer:(NSString*)layerName
{
    // make a copy of the visible overlay layers
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];

    NSMutableArray *visibleOverlayLayerNames = [NSMutableArray array];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        OverlayLayer *overlayLayer = [overlayLayersModel layerAtIndex:i];
        if (overlayLayer.visible) {
            [visibleOverlayLayerNames addObject:overlayLayer.name];
            [self hideOverlay:overlayLayer.name];
        }
    }

    // show the new base map
    [self showBaseMap:layerName];

    // add the previously visible overlay layers
    for (NSUInteger i=0; i<visibleOverlayLayerNames.count; i++) {
        [self showOverlay:[visibleOverlayLayerNames objectAtIndex:i]];
    }
    
}

#pragma mark - Overlay Layers View Controller Delegate

- (void)overlayLayersViewController:(OverlayLayersViewController *)controller didSelectLayer:(NSString*)layerName {
    [self showOverlay:layerName];
}

- (void)overlayLayersViewController:(OverlayLayersViewController *)controller didUnselectLayer:(NSString*)layerName {
    [self hideOverlay:layerName];
}

#pragma mark - Search Suggestions View Controller Delegate

// Sent when the user selects a row in the search suggestions list.
- (void)searchSuggestionsViewController:(SearchSuggestionsViewController *)controller didSelectString:(NSString *)searchString {
    NSLog(@"picked %@",searchString);
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];

    [searchBar setText:searchString];
    
    [featureHighlight hide];
    featureHighlight = nil;
    
    // get the feature that was selected
    SearchSuggestionsModel *searchSuggestionsModel = [SearchSuggestionsModel sharedInstance];
    CerFeature *feature = [searchSuggestionsModel featureWithDescription:searchString];

    // set the map to that location
    mapRegionNorthEast.latitude = feature.geometry.extent.northEast.latitude;
    mapRegionNorthEast.longitude = feature.geometry.extent.northEast.longitude;
    mapRegionSouthWest.latitude = feature.geometry.extent.southWest.latitude;
    mapRegionSouthWest.longitude = feature.geometry.extent.southWest.longitude;
    [self applyRegion];
    
    // highlight the feature
    featureHighlight = [[CerFeatureHighlight alloc] initWithCerFeature:feature andMKMapView:mkMapView andRMMapView:rmMapView];
    [self highlightFeature];
}

#pragma mark - Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar {
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    
    [searchBar setText:@""];
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar {
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)aSearchBar textDidChange:(NSString *)searchText {
    SearchSuggestionsModel *searchSuggestionsModel = [SearchSuggestionsModel sharedInstance];
    [searchSuggestionsModel suggestionsForString:searchText];

    if (searchSuggestionsModel.count > 0) {
        if (self.popoverController.isPopoverVisible) {
            // refresh popover contents
            [searchSuggestionsViewController.tableView reloadData];
        } else {
            // show popover of suggestions
            [self.popoverController setContentViewController:searchSuggestionsViewController];
            [self.popoverController setPopoverContentSize:self.popoverController.contentViewController.contentSizeForViewInPopover];
            [self.popoverController presentPopoverFromRect:[searchBar bounds] inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    } else {
        // no suggestions
        if (self.popoverController.isPopoverVisible) {
            [self.popoverController dismissPopoverAnimated:NO];
        }
    }
}

#pragma mark - Attribute Table View

- (void)highlightFeature {
    // if a feature is selected, show red overlay and attributes table
    if (featureHighlight){
        [featureHighlight show];
        attributeView.hidden = NO;
        [self.view bringSubviewToFront:attributeView];
        // update attribute view to show selected feature
        [attributeView showAttributes:featureHighlight];
    }
}

#pragma mark - UI Popover Controller Delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)controller {
    [searchBar resignFirstResponder];
}

#pragma mark - MKMapView callbacks

- (void)handleTapGesture:(UIGestureRecognizer *)gestureRecognizer {

    [searchBar resignFirstResponder];
    
    // remove any existing annotation/overlay from feature highlight
    [featureHighlight hide];
    featureHighlight = nil;
    // hide attributes view
    attributeView.hidden = YES;
    
    // get the point coordinates
    CGPoint touchPoint = [gestureRecognizer locationInView:mkMapView];
    CLLocationCoordinate2D touchMapCoordinate = [mkMapView convertPoint:touchPoint toCoordinateFromView:mkMapView];
    NSLog(@"tap at %f,%f (in view: %f,%f", touchMapCoordinate.longitude, touchMapCoordinate.latitude, touchPoint.x, touchPoint.y);
    
    // get the coordinates at 2.5mm more
    CGPoint touchPointPlus = touchPoint;
    touchPointPlus.x += 2.5 * 72.0/25.4;
    CLLocationCoordinate2D touchPlusMapCoordinate = [mkMapView convertPoint:touchPointPlus toCoordinateFromView:mkMapView];
    
    // distance tolerance for touches is 5mm, so at point on screen ± 2.5mm
    double distance = touchPlusMapCoordinate.longitude - touchMapCoordinate.longitude;
    NSLog(@"search distance = %f",distance);
    
    NSArray *features =  [SpatialSearch featuresAtLocation:touchMapCoordinate withinDistance:distance];
    for (NSUInteger i=0; i<features.count; i++) {
        CerFeature *feature = [features objectAtIndex:i];
        NSLog(@"found feature: %@ geometry:%@",feature, feature.geometry);
        featureHighlight = [[CerFeatureHighlight alloc] initWithCerFeature:feature andMKMapView:mkMapView andRMMapView:rmMapView];
        [self highlightFeature];
        break;
    }
}

#pragma mark - MKMapView Delegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    regionHasChanged = YES;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)ovl {
    if (ovl == featureHighlight.overlay) {
        return featureHighlight.overlayView;
    }
    if ([ovl isKindOfClass:[MBTilesOverlay class]]) {
        MBTilesOverlayView *view = [[MBTilesOverlayView alloc] initWithOverlay:ovl];
        view.tileAlpha = 1.0;
        return view;
    }
    return nil;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {

    // if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // check for custom annotation classes
    if ([annotation isKindOfClass:[CerFeatureAnnotation class]]) {
        // only one annotation shown at a time, so get the overlay view for it
        //return featureHighlight.annotationView;
        // jbi: does not exist anymore
        return nil;
    }

    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (MKAnnotationView* view in views) {
        if ([[view annotation] isKindOfClass:[CerFeatureAnnotation class]]) {
            [mapView selectAnnotation:[view annotation] animated:YES];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    tapGestureRecognizer.enabled = YES;
}

#pragma mark - RMMapViewDelegate Delegate

- (RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation {
    // return the RMMarker/RMCircle/RMPath that is attached as userInfo to the annotation
    return (RMMapLayer *)annotation.userInfo;
}

- (void) singleTapOnMap:(RMMapView *)map at:(CGPoint)point {

    [searchBar resignFirstResponder];

    // remove any existing annotation/overlay from feature highlight
    [featureHighlight hide];
    featureHighlight = nil;
    
    CLLocationCoordinate2D touchMapCoordinate = [rmMapView pixelToCoordinate:point];
    NSLog(@"tap at %f,%f (in view: %f,%f)", touchMapCoordinate.longitude, touchMapCoordinate.latitude, point.x, point.y);
    
    point.x += 10;
    CLLocationCoordinate2D touchPlusMapCoordinate = [rmMapView pixelToCoordinate:point];

    // distance tolerance for touches is 4 pixels, so at point on screen ± 4 pixels
    double distance = touchPlusMapCoordinate.longitude - touchMapCoordinate.longitude;
    NSLog(@"distance = %f",distance);

    NSArray *features =  [SpatialSearch featuresAtLocation:touchMapCoordinate withinDistance:distance];
    for (NSUInteger i=0; i<features.count; i++) {
        CerFeature *feature = [features objectAtIndex:i];
        NSLog(@"found feature: %@",feature);
        featureHighlight = [[CerFeatureHighlight alloc] initWithCerFeature:feature andMKMapView:mkMapView andRMMapView:rmMapView];
        [self highlightFeature];
        break;
    }    
    
}

# pragma mark - class methods

- (IBAction)homeButtonTouch:(id)sender {

    // hide any existing popover & keyboard
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];

    // reset the map
    [self homeMap];
}

- (IBAction)baseButtonTouch:(id)sender {

    // hide any existing popover & keyboard
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];
    
    // set popover contents to base layers list
    [self.popoverController setContentViewController:baseLayersViewController];
    
    // set size for this list
    [self.popoverController setPopoverContentSize:self.popoverController.contentViewController.contentSizeForViewInPopover];

    // refresh the contents of the tableview
    [[baseLayersViewController tableView] reloadData];
    
    // show popover
    [self.popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)overlaysButtonTouch:(id)sender {
    
    // hide any existing popover & keyboard
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];
    
    // set popover contents to overlay layers list
    [self.popoverController setContentViewController:overlayLayersViewController];
    
    // set size for this list
    [self.popoverController setPopoverContentSize:self.popoverController.contentViewController.contentSizeForViewInPopover];
    
    // refresh the contents of the tableview
    [[overlayLayersViewController tableView] reloadData];
    
    // show popover
    [self.popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)infoButtonTouch:(id)sender {
    // hide any existing popover & keyboard
    if (self.popoverController.isPopoverVisible) {
        [self.popoverController dismissPopoverAnimated:NO];
    }
    [searchBar resignFirstResponder];

    SetupViewController *setupViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupVC"];
    setupViewController.delegate = self;
    [self presentModalViewController:setupViewController animated:YES];
}

/**
 * set the layer visibilities and map region to the startup configuration
 */
- (void)homeMap {
    AppConfiguration *appConfig = [AppConfiguration sharedInstance];

    // reset all visibility in the model
    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    for (NSUInteger i=0; i<baseLayersModel.count; i++) {
        [[baseLayersModel layerAtIndex:i] setVisible:NO];
    }
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    for (NSUInteger i=0; i<overlayLayersModel.count; i++) {
        [[overlayLayersModel layerAtIndex:i] setVisible:NO];
    }
    
    // set configured region
    mapRegionNorthEast.latitude = appConfig.homeRegionNorthEast.latitude;
    mapRegionNorthEast.longitude = appConfig.homeRegionNorthEast.longitude;
    mapRegionSouthWest.latitude = appConfig.homeRegionSouthWest.latitude;
    mapRegionSouthWest.longitude = appConfig.homeRegionSouthWest.longitude;
    regionHasChanged = NO;  // the user didn't change it - we did

    // turn on the configured layers
    NSArray *homeSessionsLayers = [appConfig homeLayers];
    for (NSUInteger i=0; i<homeSessionsLayers.count; i++) {
        NSString *layerName = [homeSessionsLayers objectAtIndex:i];
        if ([self showBaseMap:layerName]) {
            // set the visibility to on
            BaseLayer *baseLayer = [baseLayersModel layerWithName:layerName];
            baseLayer.visible = YES;
        } else {
            // not a base map layer name - try overlay layers
            if ([self showOverlay:layerName]) {
                // set the visibility to on
                OverlayLayer *overlayLayer = [overlayLayersModel layerWithName:layerName];
                overlayLayer.visible = YES;
            } else {
                // bad config
                NSLog(@"layer %@ doesn't exist",layerName);
            }
        }
    }
    
    // now apply configured region
    [self applyRegion];
    
}

- (BOOL)showBaseMap:(NSString*)name {
    BaseLayersModel *baseLayersModel = [BaseLayersModel sharedInstance];
    BaseLayer *baseLayer = [baseLayersModel layerWithName:name];
    if (baseLayer) {
        // read the region of the current basemap
        if (regionHasChanged) {
            [self storeRegion];
        }

        // hide any feature highlight
        [featureHighlight hide];

        // swap to new basemap
        if ([baseLayer.type isEqualToString:@"mapkit"]) {
            [self showMapKitMapWithSubType:baseLayer.subtype];
            
        } else if ([baseLayer.type isEqualToString:@"route-me"]) {
            [self showRouteMeMapWithSubType:baseLayer.subtype andUrl:baseLayer.url andPath:baseLayer.path];
            
        }
        
        // update region of map if it has changed from last set
        if (regionHasChanged) {
            [self applyRegion];
        }

        // redisplay any feature highlight
        [self highlightFeature];
        return YES;
    }
    return NO;
    
}

- (BOOL)showOverlay:(NSString*)name {
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    OverlayLayer *overlayLayer = [overlayLayersModel layerWithName:name];
    if (overlayLayer) {
        if ([overlayLayer.subtype isEqualToString:@"mbtiles"] || [overlayLayer.subtype isEqualToString:@"mbtiles++"]) {
            if (mkMapView.hidden == NO) {
                // show over Map Kit map
                if (!overlayLayer.mkOverlay) {
                    MBTilesOverlay *tileOverlay = [[MBTilesOverlay alloc] initOverlay];
                    tileOverlay.fmdb = overlayLayer.fmdb;
                    overlayLayer.mkOverlay = tileOverlay;
                }
                [mkMapView addOverlay:overlayLayer.mkOverlay];

            } else if (rmMapView.hidden == NO) {
                // show over Route-Me Map;
                if (!overlayLayer.rmOverlay) {
                    overlayLayer.rmOverlay = [[RMMBTilesTileSource alloc] initWithTileSetURL:[NSURL fileURLWithPath:overlayLayer.path]];
                    [rmMapView addTileSource:overlayLayer.rmOverlay];
                }
                [rmMapView setHidden:NO forTileSource:overlayLayer.rmOverlay];

            }
            overlayLayer.visible = YES;
            return YES;
        } else if ([overlayLayer.type isEqualToString:@"traffic"]) {
            // TODO
            NSLog(@"Not yet implemented");
            return YES;
            
        } else if ([overlayLayer.type isEqualToString:@"kml"]) {
            // TODO
            NSLog(@"Not yet implemented");
            return YES;
        }
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)hideOverlay:(NSString*)name {
    OverlayLayersModel *overlayLayersModel = [OverlayLayersModel sharedInstance];
    OverlayLayer *overlayLayer = [overlayLayersModel layerWithName:name];
    if (overlayLayer) {
        if ([overlayLayer.subtype isEqualToString:@"mbtiles"] || [overlayLayer.subtype isEqualToString:@"mbtiles++"]) {
            if (mkMapView.hidden == NO) {
                // hide overlay from Map Kit map
                [mkMapView removeOverlay:overlayLayer.mkOverlay];
            } else if (rmMapView.hidden == NO) {
                // hide overlay from Route-Me map
                [rmMapView setHidden:YES forTileSource:overlayLayer.rmOverlay];
            }
            overlayLayer.visible = NO;
            return YES;
        } else if ([overlayLayer.type isEqualToString:@"traffic"]) {
            // TODO
            NSLog(@"Not yet implemented");
            return YES;
            
        } else if ([overlayLayer.type isEqualToString:@"kml"]) {
            // TODO
            NSLog(@"Not yet implemented");
            return YES;
        }
        return YES;
    } else {
        return NO;
    }
}

- (void)showMapKitMapWithSubType:(NSString*)subtype {
    // set map type
    if ([subtype isEqualToString:@"map"]) {
        mkMapView.mapType = MKMapTypeStandard;
    } else if ([subtype isEqualToString:@"satellite"]) {
        mkMapView.mapType = MKMapTypeSatellite;
    } else if ([subtype isEqualToString:@"hybrid"]) {
        mkMapView.mapType = MKMapTypeHybrid;
    } else {
        mkMapView.mapType = MKMapTypeStandard;
    }

    mkMapView.hidden = NO;
    rmMapView.hidden = YES;
}

- (void)showRouteMeMapWithSubType:(NSString*)subtype andUrl:(NSURL*)url andPath:(NSString*)path {

    id <RMTileSource> tileSource;
    if ([subtype isEqualToString:@"openstreetmap"]) {
        tileSource = [[RMOpenStreetMapSource alloc] init];
    } else if ([subtype isEqualToString:@"openaerial"]) {
        tileSource = [[RMMapQuestOSMSource alloc] init];
    } else if ([subtype isEqualToString:@"mbtiles"]) {
        tileSource = [[RMMBTilesTileSource alloc] initWithTileSetURL:[NSURL fileURLWithPath:path]];
    } else {
        tileSource = [[RMOpenStreetMapSource alloc] init];
    }
    NSLog(@"tileSource:%@ minZoom:%f maxZoom:%f",[tileSource description], [tileSource minZoom], [tileSource maxZoom]);
    [rmMapView setTileSource:tileSource];
    
    mkMapView.hidden = YES;
    rmMapView.hidden = NO;
}

/**
 * read the region from the active map
 */
- (void)storeRegion {
    if (mkMapView.hidden == NO) {
        // get region from Map Kit map
        mapRegionNorthEast.latitude  = mkMapView.region.center.latitude  + mkMapView.region.span.latitudeDelta/2.0;
        mapRegionNorthEast.longitude = mkMapView.region.center.longitude + mkMapView.region.span.longitudeDelta/2.0;
        mapRegionSouthWest.latitude  = mkMapView.region.center.latitude  - mkMapView.region.span.latitudeDelta/2.0;
        mapRegionSouthWest.longitude = mkMapView.region.center.longitude - mkMapView.region.span.longitudeDelta/2.0;
    } else if (rmMapView.hidden == NO) {
        // get region from Route-Me Map;
        mapRegionNorthEast.latitude  = rmMapView.latitudeLongitudeBoundingBox.northEast.latitude;
        mapRegionNorthEast.longitude = rmMapView.latitudeLongitudeBoundingBox.northEast.longitude;
        mapRegionSouthWest.latitude  = rmMapView.latitudeLongitudeBoundingBox.southWest.latitude;
        mapRegionSouthWest.longitude = rmMapView.latitudeLongitudeBoundingBox.southWest.longitude;
    }
    
}

/**
 * set the region to all maps
 */
- (void)applyRegion {
    // set region on Map Kit map
    MKCoordinateRegion mkRegion;
    mkRegion.center.latitude = (mapRegionNorthEast.latitude + mapRegionSouthWest.latitude)/2.0;
    mkRegion.center.longitude = (mapRegionNorthEast.longitude + mapRegionSouthWest.longitude)/2.0;
    mkRegion.span.latitudeDelta = mapRegionNorthEast.latitude - mapRegionSouthWest.latitude;
    mkRegion.span.longitudeDelta = mapRegionNorthEast.longitude - mapRegionSouthWest.longitude;
    mkMapView.region = mkRegion;
    
    // set region on Route-Me maps
    [rmMapView zoomWithLatitudeLongitudeBoundsSouthWest:mapRegionSouthWest northEast:mapRegionNorthEast animated:YES];
    
    // reset flag
    regionHasChanged = NO;
}

@end
